import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * Metode za analizu izraza (poglavlje 4.4.4. u uputama)
 * @author wex
 *
 */
public abstract class Expression {
	
	/**
	 * Pomocna metoda, provjerava relaciju (type ~ int) citaj uputu...
	 * @author wex
	 * @param type
	 * @return
	 */
	public static boolean isImplicitlyCastableToInt(String type){
		return type.equals("int") || type.equals("char") || type.equals("const(int)") || type.equals("const(char)");
	}
	
	/**
	 * Pomocna metoda, provjerava jel moguce castati (implicitno ili eksplicitno)
	 * iz tipa fromType u tip toType, poglavlje 4.3.1 u uputi...
	 * 
	 * @param fromType
	 * @param toType
	 * @return
	 */
	public static boolean isCastable(String fromType, String toType){
		if(fromType.equals("int") && toType.equals("char"))
			return true;
		if(fromType.equals("int") && toType.equals("const(char)"))
			return true;
		if(fromType.equals("const(int)") && toType.equals("const(char)"))
			return true;
		if(fromType.equals("const(int)") && toType.equals("char"))
			return true;
		if(isImplicitlyCastable(fromType, toType))
			return true;
		return false;
	}
	
	/**
	 * Pomocna metoda, provjerava jel moguce implicitno castati iz tipa
	 * fromType u tip toType...
	 * 
	 * @param fromType
	 * @param toType
	 * @return
	 */
	public static boolean isImplicitlyCastable(String fromType, String toType){
		if(fromType.equals(toType))
			return true;
		if(fromType.equals("char"))
			if(toType.equals("int") || toType.equals("const(char)") || toType.equals("const(int)"))
				return true;
		if(fromType.equals("const(char)"))
			if(toType.equals("int") || toType.equals("char") || toType.equals("const(int)"))
				return true;
		if(fromType.equals("int"))
			if(toType.equals("const(int)"))
				return true;
		if(fromType.equals("const(int)"))
			if(toType.equals("int"))
				return true;
		if(fromType.equals("niz(int)"))
			if(toType.equals("niz(const(int))"))
				return true;
		if(fromType.equals("niz(char)"))
			if(toType.equals("niz(const(char))"))
				return true;
		return false;
	}
	
	public static char getCharIfEscaped(String s){
		if(s.length() == 1)
			return s.charAt(0);
		
		char ret = s.charAt(1);
		
		switch (ret){
		case 't': return '\t';
		case 'n': return '\n';
		case '0': return '\0';
		case '\'': return '\'';
		case '"': return '"';
		case '\\': return '\\';
		}
		return '\0';
	}
	
	public static String oct2dec(String oct){
		int dec = 0;
		int curExp = 1;
		for(int i = oct.length() - 1; i >= 0; i--, curExp *= 8)
			dec += (Integer.parseInt(oct.substring(i, i+1)) * curExp);
		return Integer.valueOf(dec).toString();
	}
	
	/**
	 * Nakon poziva ove metode ona stavlja prvo l_izraz na stog pa onda tip
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean primarni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("IDN")){
			String name = node.getChild(0).getLex_atom();
			String label = Analyzator.currentScope.findLabel(name);
			if(label != null){
				if(Analyzator.curFunction.localVars.contains(label)){
					int offset = Analyzator.curFunction.localVars.indexOf(label);
					AsGen.addAssembler(AsGen.load("R0", "(R5 - %D " + Integer.valueOf(4*(offset+1)).toString() + ")"));
					AsGen.addAssembler(AsGen.push("R0"));
				}
				else if(Analyzator.curFunction.params.contains(label)){
					int offset = Analyzator.curFunction.params.indexOf(label);
					AsGen.addAssembler(AsGen.load("R0", "(R5 + %D " + Integer.valueOf(4*(Analyzator.curFunction.params.size()-offset+1)).toString() + ")"));
					AsGen.addAssembler(AsGen.push("R0"));
				}
			}
			else{
				AsGen.addAssembler(AsGen.load("R0", "(VAR_" + name + ")"));
				AsGen.addAssembler(AsGen.push("R0"));
			}
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("BROJ")){
			VarFRISC con = VarFRISC.createNewConst();
			
			String val = node.getChild(0).getLex_atom();
			if(val.startsWith("0") && val.length() > 1){
				if(val.startsWith("0x"))
					con.initValue = "0" + val.substring(2);
				else
					con.initValue = "%D " + oct2dec(val.substring(1));
			}
			else
				con.initValue = "%D " + val;
			
			Analyzator.varsFRISC.add(con);
			AsGen.addAssembler(AsGen.load("R0", "(" + con.label + ")"));
			AsGen.addAssembler(AsGen.push("R0"));
		}
		
		// 3. produkcija
		else if(node.getChild(0).getName().equals("ZNAK")){
			String s = node.getChild(0).getLex_atom();
			int c = getCharIfEscaped(s.substring(1, s.length()-1));
			
			VarFRISC con = VarFRISC.createNewConst();
			con.initValue = "%D " + Integer.valueOf(c).toString();
			Analyzator.varsFRISC.add(con);
			AsGen.addAssembler(AsGen.load("R0", "(" + con.label + ")"));
			AsGen.addAssembler(AsGen.push("R0"));
		}
		
		// 4. produkcija 
		else if(node.getChild(0).getName().equals("NIZ_ZNAKOVA")){
			// TODO OVO JE POSEBNI SLUCAJ PRI GENERIRANJU FRISC KODA!!! TREBA SE RJESAVATI U NEKOM CVORU RODITELJU!
		}
		
		// 5. produkcija
		else if(node.getChild(0).getName().equals("L_ZAGRADA")){
			izraz(node.getChild(1));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean postfiks_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("<primarni_izraz>")){
			Expression.primarni_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChild(1).getName().equals("L_UGL_ZAGRADA")){
			String name = node.getChild(0).getChild(0).getChild(0).getLex_atom();
			izraz(node.getChild(2));
			AsGen.addAssembler(AsGen.pop("R1"));
			AsGen.addAssembler(AsGen.shl("R1", "2", "R1"));
			String label = Analyzator.currentScope.findLabel(name);
			if(label != null){
				if(Analyzator.curFunction.localVars.contains(label)){
					int offset = Analyzator.curFunction.localVars.indexOf(label)+1;
					AsGen.addAssembler(AsGen.move("%D " + Integer.valueOf(4*(offset+1)).toString(), "R2"));
					AsGen.addAssembler(AsGen.sub("R5", "R2", "R2"));
					AsGen.addAssembler(AsGen.sub("R2", "R1", "R2"));
					AsGen.addAssembler(AsGen.load("R0", "(R2)"));
					AsGen.addAssembler(AsGen.push("R0"));
				}
				else if(Analyzator.curFunction.params.contains(label)){
					int offset = Analyzator.curFunction.params.indexOf(label);
					AsGen.addAssembler(AsGen.load("R2", "(R5 + %D " + Integer.valueOf(4*(Analyzator.curFunction.params.size()-offset+1)).toString() + ")"));
					AsGen.addAssembler(AsGen.sub("R2", "R1", "R2"));
					AsGen.addAssembler(AsGen.load("R0", "(R2)"));
					AsGen.addAssembler(AsGen.push("R0"));
				}
			}
			else{
				AsGen.addAssembler(AsGen.load("R2", "(VAR_" + name + ")"));
				AsGen.addAssembler(AsGen.add("R2", "R1", "R2"));
				AsGen.addAssembler(AsGen.load("R0", "(R2)"));
				AsGen.addAssembler(AsGen.push("R0"));
			}
		}
		
		// 3. produkcija
		else if(node.getChild(1).getName().equals("L_ZAGRADA") && node.getChild(2).getName().equals("D_ZAGRADA")){
			String name = node.getChild(0).getChild(0).getChild(0).getLex_atom();
			AsGen.addAssembler(AsGen.call(name));//pozovi potprogram
			AsGen.addAssembler(AsGen.push("R6"));//pushaj ono sto ti vrati na stog
		}
		
		// 4. produkcija
		else if(node.getChild(1).getName().equals("L_ZAGRADA") && node.getChild(2).getName().equals("<lista_argumenata>")){
			String name = node.getChild(0).getChild(0).getChild(0).getLex_atom();
			lista_argumenata(node.getChild(2));
			AsGen.addAssembler(AsGen.call(name));//pozovi potprogram
			AsGen.addAssembler(AsGen.add("R7", "%D " + Integer.valueOf(4 * Analyzator.functionsFRISC.get(name).params.size()).toString(), "R7"));
			AsGen.addAssembler(AsGen.push("R6"));//pushaj ono sto ti vrati na stog
		}
		
		// 5. i 6. produkcija
		else if(node.getChild(1).getName().equals("OP_INC") || node.getChild(1).getName().equals("OP_DEC")){
			if(node.getChild(0).getChild(0).getName().equals("<postfiks_izraz>")){ //ako ubacujem u array
				izraz(node.getChild(0).getChild(0).getChild(2));
				AsGen.addAssembler(AsGen.pop("R1"));
				AsGen.addAssembler(AsGen.shl("R1", "2", "R1"));
				String name = node.getChild(0).getChild(0).getChild(0).getChild(0).getLex_atom();
				String label = Analyzator.currentScope.findLabel(name);
				if(label != null){
					if(Analyzator.curFunction.localVars.contains(label)){
						int offset = Analyzator.curFunction.localVars.indexOf(label);
						AsGen.addAssembler(AsGen.move("%D " + Integer.valueOf(4*(offset+1)).toString(), "R2"));
						AsGen.addAssembler(AsGen.sub("R5", "R2", "R2"));
						AsGen.addAssembler(AsGen.add("R1", "R2", "R1"));
						AsGen.addAssembler(AsGen.load("R0", "(R1)"));
						AsGen.addAssembler(AsGen.push("R0"));
						if(node.getChild(0).getName().equals("OP_INC"))
							AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
						else
							AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
						AsGen.addAssembler(AsGen.store("R0", "(R1)"));
					}
					else{
						int offset = Analyzator.curFunction.params.indexOf(label);
						AsGen.addAssembler(AsGen.move("%D " + Integer.valueOf(4*(Analyzator.curFunction.params.size()-offset+1)).toString(), "R2"));
						AsGen.addAssembler(AsGen.add("R5", "R2", "R2"));
						AsGen.addAssembler(AsGen.load("R2", "(R2)"));
						AsGen.addAssembler(AsGen.sub("R2", "R1", "R1"));
						AsGen.addAssembler(AsGen.load("R0", "(R1)"));
						AsGen.addAssembler(AsGen.push("R0"));
						if(node.getChild(0).getName().equals("OP_INC"))
							AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
						else
							AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
						AsGen.addAssembler(AsGen.store("R0", "(R1)"));
					}
				}
				else{
					AsGen.addAssembler(AsGen.load("R2", "(VAR_" + name + ")"));
					AsGen.addAssembler(AsGen.add("R2", "R1", "R2"));
					AsGen.addAssembler(AsGen.load("R0", "(R1)"));
					AsGen.addAssembler(AsGen.push("R0"));
					if(node.getChild(0).getName().equals("OP_INC"))
						AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
					else
						AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
					AsGen.addAssembler(AsGen.store("R0", "(R2)"));
				}
			}
			else{ //ako je obicna varijabla
				String name = node.getChild(0).getChild(0).getChild(0).getLex_atom();
				String label = Analyzator.currentScope.findLabel(name);
				if(label != null){
					if(Analyzator.curFunction.localVars.contains(label)){
						int offset = Analyzator.curFunction.localVars.indexOf(label);
						AsGen.addAssembler(AsGen.load("R0", "(R5 - %D " + Integer.valueOf(4*(offset+1)).toString() + ")"));
						AsGen.addAssembler(AsGen.push("R0"));
						if(node.getChild(0).getName().equals("OP_INC"))
							AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
						else
							AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
						AsGen.addAssembler(AsGen.store("R0", "(R5 - %D " + Integer.valueOf(4*(offset+1)).toString() + ")"));
					}
					else if(Analyzator.curFunction.params.contains(label)){
						int offset = Analyzator.curFunction.params.indexOf(label);
						AsGen.addAssembler(AsGen.load("R0", "(R5 + %D " + Integer.valueOf(4*(Analyzator.curFunction.params.size()-offset+1)).toString() + ")"));
						AsGen.addAssembler(AsGen.push("R0"));
						if(node.getChild(0).getName().equals("OP_INC"))
							AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
						else
							AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
						AsGen.addAssembler(AsGen.store("R0", "(R5 + %D " + Integer.valueOf(4*(Analyzator.curFunction.params.size()-offset+1)).toString() + ")"));
					}
				}
				else{
					AsGen.addAssembler(AsGen.load("R0", "(VAR_" + name + ")"));
					AsGen.addAssembler(AsGen.push("R0"));
					if(node.getChild(0).getName().equals("OP_INC"))
						AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
					else
						AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
					AsGen.addAssembler(AsGen.store("R0", "(VAR_" + name + ")"));
				}
			}
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean lista_argumenata(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			izraz_pridruzivanja(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			lista_argumenata(node.getChild(0));
			izraz_pridruzivanja(node.getChild(2));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean unarni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.postfiks_izraz(node.getChild(0));
		}
		
		// 2. i 3. produkcija
		else if(node.getChild(1).getName().equals("<unarni_izraz>")){
			if(node.getChild(1).getChild(0).getChild(0).getName().equals("<postfiks_izraz>")){ //ako ubacujem u array
				izraz(node.getChild(1).getChild(0).getChild(2));
				AsGen.addAssembler(AsGen.pop("R1"));
				AsGen.addAssembler(AsGen.shl("R1", "2", "R1"));
				String name = node.getChild(1).getChild(0).getChild(0).getChild(0).getChild(0).getLex_atom();
				String label = Analyzator.currentScope.findLabel(name);
				if(label != null){
					if(Analyzator.curFunction.localVars.contains(label)){
						int offset = Analyzator.curFunction.localVars.indexOf(label);
						AsGen.addAssembler(AsGen.move("%D " + Integer.valueOf(4*(offset+1)).toString(), "R2"));
						AsGen.addAssembler(AsGen.sub("R5", "R2", "R2"));
						AsGen.addAssembler(AsGen.add("R1", "R2", "R1"));
						AsGen.addAssembler(AsGen.load("R0", "(R1)"));
						if(node.getChild(0).getName().equals("OP_INC"))
							AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
						else
							AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
						AsGen.addAssembler(AsGen.push("R0"));
						AsGen.addAssembler(AsGen.store("R0", "(R1)"));
					}
					else{
						int offset = Analyzator.curFunction.params.indexOf(label);
						AsGen.addAssembler(AsGen.move("%D " + Integer.valueOf(4*(Analyzator.curFunction.params.size()-offset+1)).toString(), "R2"));
						AsGen.addAssembler(AsGen.add("R5", "R2", "R2"));
						AsGen.addAssembler(AsGen.load("R2", "(R2)"));
						AsGen.addAssembler(AsGen.sub("R2", "R1", "R1"));
						AsGen.addAssembler(AsGen.load("R0", "(R1)"));
						if(node.getChild(0).getName().equals("OP_INC"))
							AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
						else
							AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
						AsGen.addAssembler(AsGen.push("R0"));
						AsGen.addAssembler(AsGen.store("R0", "(R1)"));
					}
				}
				else{
					AsGen.addAssembler(AsGen.load("R2", "(VAR_" + name + ")"));
					AsGen.addAssembler(AsGen.add("R2", "R1", "R2"));
					AsGen.addAssembler(AsGen.load("R0", "(R1)"));
					if(node.getChild(0).getName().equals("OP_INC"))
						AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
					else
						AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
					AsGen.addAssembler(AsGen.push("R0"));
					AsGen.addAssembler(AsGen.store("R0", "(R2)"));
				}
			}

			else{ //ako je obicna varijabla
				String name = node.getChild(1).getChild(0).getChild(0).getChild(0).getLex_atom();
				String label = Analyzator.currentScope.findLabel(name);
				if(label != null){
					if(Analyzator.curFunction.localVars.contains(label)){
						int offset = Analyzator.curFunction.localVars.indexOf(label);
						AsGen.addAssembler(AsGen.load("R0", "(R5 - %D " + Integer.valueOf(4*(offset+1)).toString() + ")"));
						if(node.getChild(0).getName().equals("OP_INC"))
							AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
						else
							AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
						AsGen.addAssembler(AsGen.push("R0"));
						AsGen.addAssembler(AsGen.store("R0", "(R5 - %D " + Integer.valueOf(4*(offset+1)).toString() + ")"));
					}
					else if(Analyzator.curFunction.params.contains(label)){
						int offset = Analyzator.curFunction.params.indexOf(label);
						AsGen.addAssembler(AsGen.load("R0", "(R5 + %D " + Integer.valueOf(4*(Analyzator.curFunction.params.size()-offset+1)).toString() + ")"));
						if(node.getChild(0).getName().equals("OP_INC"))
							AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
						else
							AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
						AsGen.addAssembler(AsGen.push("R0"));
						AsGen.addAssembler(AsGen.store("R0", "(R5 + %D " + Integer.valueOf(4*(Analyzator.curFunction.params.size()-offset+1)).toString() + ")"));
					}
				}
				else{
					AsGen.addAssembler(AsGen.load("R0", "(VAR_" + name + ")"));
					if(node.getChild(0).getName().equals("OP_INC"))
						AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
					else
						AsGen.addAssembler(AsGen.sub("R0", "1", "R0"));
					AsGen.addAssembler(AsGen.push("R0"));
					AsGen.addAssembler(AsGen.store("R0", "(VAR_" + name + ")"));
				}
			}
		}
		
		// 4. produkcija
		else if(node.getChild(1).getName().equals("<cast_izraz>")){
			cast_izraz(node.getChild(1));
			String op = node.getChild(0).getChild(0).getName();
			
			AsGen.addAssembler(AsGen.pop("R0"));
			if(op.equals("MINUS")){
				AsGen.addAssembler(AsGen.xor("R0", "-1", "R0"));
				AsGen.addAssembler(AsGen.add("R0", "1", "R0"));
			}
			
			else if(op.equals("OP_TILDA")){
				AsGen.addAssembler(AsGen.xor("R0", "-1", "R0"));
			}
			
			else if(op.equals("OP_NEG")){
				 AsGen.addAssembler(AsGen.cmp("R0", "0"));
				 String lab1 = AsGen.getJumpLabel();
				 String lab2 = AsGen.getJumpLabel();
				 
				 AsGen.addAssembler(AsGen.jp("EQ", lab1));
				 AsGen.addAssembler(AsGen.move("0", "R0"));
				 AsGen.addAssembler(AsGen.jp(null, lab2));
				 AsGen.addAssembler(lab1 + AsGen.move("1", "R0"));
				 AsGen.addAssembler(lab2 + AsGen.push("R0"));
			}
			
			AsGen.addAssembler(AsGen.push("R0"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean cast_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.unarni_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 4){
			cast_izraz(node.getChild(3));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean ime_tipa(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			// nebitno za generiranje frisc koda
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 2){
			// nebitno za generiranje frisc koda
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean specifikator_tipa(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("KR_VOID")){
			// nebitno za generiranje frisc koda
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("KR_CHAR")){
			// nebitno za generiranje frisc koda
		}
		
		// 3. produkcija
		else if(node.getChild(0).getName().equals("KR_INT")){
			// nebitno za generiranje frisc koda
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean multiplikativni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.cast_izraz(node.getChild(0));
		}
		
		// 2. 3. i 4. produkcija
		else if(node.getChildrenAmount() == 3){
			multiplikativni_izraz(node.getChild(0));
			cast_izraz(node.getChild(2));
			
			if(node.getChild(1).getName().equals("OP_PUTA"))
				AsGen.addAssembler(" CALL MUL\n");
			else if(node.getChild(1).getName().equals("OP_DIJELI"))
				AsGen.addAssembler(" CALL DIV\n");
			else if(node.getChild(1).getName().equals("OP_MOD"))
				AsGen.addAssembler(" CALL MOD\n");
			
			AsGen.addAssembler(AsGen.push("R6"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean aditivni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.multiplikativni_izraz(node.getChild(0));
		}
		
		// 2. i 3. produkcija
		else if(node.getChildrenAmount() == 3){
			aditivni_izraz(node.getChild(0));
			multiplikativni_izraz(node.getChild(2));
			
			AsGen.addAssembler(AsGen.pop("R1"));
			AsGen.addAssembler(AsGen.pop("R0"));
			if(node.getChild(1).getName().equals("PLUS"))
				AsGen.addAssembler(AsGen.add("R0", "R1", "R0"));
			else
				AsGen.addAssembler(AsGen.sub("R0", "R1", "R0"));
			AsGen.addAssembler(AsGen.push("R0"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean odnosni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.aditivni_izraz(node.getChild(0));
		}
		
		// 2. 3. 4. i 5. produkcija
		else if(node.getChildrenAmount() == 3){
			odnosni_izraz(node.getChild(0));
			aditivni_izraz(node.getChild(2));
			AsGen.addAssembler(AsGen.move("1", "R2"));
			AsGen.addAssembler(AsGen.pop("R1"));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.cmp("R0", "R1"));
			
			String op = node.getChild(1).getName();
			String jmpLabel = AsGen.getJumpLabel();
			
			if(op.equals("OP_LT"))
				AsGen.addAssembler(AsGen.jp("SLT", jmpLabel));
			else if(op.equals("OP_GT"))
				AsGen.addAssembler(AsGen.jp("SGT", jmpLabel));
			else if(op.equals("OP_LTE"))
				AsGen.addAssembler(AsGen.jp("SLE", jmpLabel));
			else if(op.equals("OP_GTE"))
				AsGen.addAssembler(AsGen.jp("SGE", jmpLabel));
			AsGen.addAssembler(AsGen.move("0", "R2"));
			AsGen.addAssembler(jmpLabel + AsGen.push("R2")); 
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean jednakosni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.odnosni_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			jednakosni_izraz(node.getChild(0));
			odnosni_izraz(node.getChild(2));
			AsGen.addAssembler(AsGen.move("1", "R2"));
			AsGen.addAssembler(AsGen.pop("R1"));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.cmp("R0", "R1"));
			String label = AsGen.getJumpLabel();
			AsGen.addAssembler(AsGen.jp("EQ", label));
			AsGen.addAssembler(AsGen.move("0", "R2"));
			AsGen.addAssembler(label + AsGen.push("R2"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean bin_i_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.jednakosni_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			bin_i_izraz(node.getChild(0));
			jednakosni_izraz(node.getChild(2));
			
			AsGen.addAssembler(AsGen.pop("R1"));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.and("R0", "R1", "R0"));
			AsGen.addAssembler(AsGen.push("R0"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean bin_xili_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.bin_i_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			bin_xili_izraz(node.getChild(0));
			bin_i_izraz(node.getChild(2));
			
			AsGen.addAssembler(AsGen.pop("R1"));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.xor("R0", "R1", "R0"));
			AsGen.addAssembler(AsGen.push("R0"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean bin_ili_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.bin_xili_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			bin_ili_izraz(node.getChild(0));
			bin_xili_izraz(node.getChild(2));
			
			AsGen.addAssembler(AsGen.pop("R1"));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.or("R0", "R1", "R0"));
			AsGen.addAssembler(AsGen.push("R0"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean log_i_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.bin_ili_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			String skipLabel = AsGen.getJumpLabel();
			log_i_izraz(node.getChild(0));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.cmp("R0", "0"));
			AsGen.addAssembler(AsGen.jp("EQ", skipLabel));
			bin_ili_izraz(node.getChild(2));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(skipLabel);
			AsGen.addAssembler(AsGen.push("R0"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean log_ili_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.log_i_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			String skipLabel = AsGen.getJumpLabel();
			log_ili_izraz(node.getChild(0));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.cmp("R0", "0"));
			AsGen.addAssembler(AsGen.jp("NE", skipLabel));
			log_i_izraz(node.getChild(2));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(skipLabel);
			AsGen.addAssembler(AsGen.push("R0"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean izraz_pridruzivanja(TreeNode node){
		
		// 1. produkcija 
		if(node.getChildrenAmount() == 1){
			Expression.log_ili_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			izraz_pridruzivanja(node.getChild(2));
			//AsGen.addAssembler(AsGen.pop("R0")); NE TU OVO JER CE SE POBIT NAKON STO GENERIRAS <IZRAZ>
			
			if(node.getChild(0).getChild(0).getName().equals("<postfiks_izraz>")){ //ako ubacujem u array
				izraz(node.getChild(0).getChild(2));
				AsGen.addAssembler(AsGen.pop("R1"));
				AsGen.addAssembler(AsGen.pop("R0")); //TU DA!
				AsGen.addAssembler(AsGen.shl("R1", "2", "R1"));
				String name = node.getChild(0).getChild(0).getChild(0).getChild(0).getLex_atom();
				String label = Analyzator.currentScope.findLabel(name);
				if(label != null){
					if(Analyzator.curFunction.localVars.contains(label)){
						int offset = Analyzator.curFunction.localVars.indexOf(label);
						AsGen.addAssembler(AsGen.move("%D " + Integer.valueOf(4*(offset+1)).toString(), "R2"));
						AsGen.addAssembler(AsGen.sub("R5", "R2", "R2"));
						AsGen.addAssembler(AsGen.add("R1", "R2", "R1"));
						AsGen.addAssembler(AsGen.store("R0", "(R1)"));
					}
					else{
						int offset = Analyzator.curFunction.params.indexOf(label);
						AsGen.addAssembler(AsGen.move("%D " + Integer.valueOf(4*(Analyzator.curFunction.params.size()-offset+1)).toString(), "R2"));
						AsGen.addAssembler(AsGen.add("R5", "R2", "R2"));
						AsGen.addAssembler(AsGen.load("R2", "(R2)"));
						AsGen.addAssembler(AsGen.sub("R2", "R1", "R1"));
						AsGen.addAssembler(AsGen.store("R0", "(R1)"));
					}
				}
				else{
					AsGen.addAssembler(AsGen.load("R2", "(VAR_" + name + ")"));
					AsGen.addAssembler(AsGen.add("R2", "R1", "R2"));
					AsGen.addAssembler(AsGen.store("R0", "(R2)"));
				}
			}
			
			else{ //ako je obicna varijabla
				AsGen.addAssembler(AsGen.pop("R0")); //TAKODER I TU DA!
				String name = node.getChild(0).getChild(0).getChild(0).getLex_atom();
				String label = Analyzator.currentScope.findLabel(name);
				if(label != null){
					if(Analyzator.curFunction.localVars.contains(label)){
						int offset = Analyzator.curFunction.localVars.indexOf(label);
						AsGen.addAssembler(AsGen.store("R0", "(R5 - %D " + Integer.valueOf(4*(offset+1)).toString() + ")"));
					}
					else if(Analyzator.curFunction.params.contains(label)){
						int offset = Analyzator.curFunction.params.indexOf(label);
						AsGen.addAssembler(AsGen.store("R0", "(R5 + %D " + Integer.valueOf(4*(Analyzator.curFunction.params.size()-offset+1)).toString() + ")"));
					}
				}
				else{
					AsGen.addAssembler(AsGen.store("R0", "(VAR_" + name + ")"));
				}
			}
			
			AsGen.addAssembler(AsGen.push("R0"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			Expression.izraz_pridruzivanja(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			izraz(node.getChild(0));
			izraz_pridruzivanja(node.getChild(2));
		}
		
		return true;
	}

}
