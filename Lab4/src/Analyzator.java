import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;



/**
 * Tu ide staticka metoda za pokretanje analize koja ce pozvati analizu na znak koji je korijen u genStablu.
 * Podijelit cemo metode kako su podijeljene u uputama
 * 		- izrazi (poglavlje 4.4.4.) klasa Expression
 * 		- naredbe (poglavlje 4.4.5.) klasa Command
 * 		- deklaracije/definicije (poglavlje 4.4.6.) klasa Declaration
 * Ove klase ce biti apstraktne i imat ce samo staticke metode, u pocetku bi bilo dobro da svatko pise
 * metode u jednu od tih klasa pa kasnije onda dovrsavamo zajedno ako netko zaostane.
 * Ako cemo pisati u istu klasu vise nas onda onaj koji merge-a samo mora pripaziti i sve ce bit oke.
 * 
 * U ovu klasu Analyzator stavljajte i podatkovne strukture koje ce biti potrebne za globalnu komunikaciju i slicno.
 * Kad god mozete saljite potrebne parametre preko argumenata poziva metoda.
 * Na svaku metodu napisite "@author ..."!
 * 
 * NOTE: Znam da je ova metoda "start" nepotrebna (jer postoji samo jedan pocetni nezavrsni znak 
 * gramatike pa ne treba ni provjeravati ovo sve sa if-ovima, al ovako barem vidimo sta je implementirano a sta ne)
 * Kasnije mozemo maknuti ovu metodu, tj ove sve if-ove i samo staviti da poziva direktno analizu pocetnog nezavrsnog znaka.
 * 
 * NOTE2: Svaka metoda mora imati parametar TreeNode, da "zna" gdje se trenutno nalazimo u generativnom stablu
 * i da zna o kojoj produkciji se radi!
 * @author wex
 *
 */
public abstract class Analyzator {
	//Ovdje stavite ako su vam potrebne neke globalne podatkovne strukture
	public static Scope currentScope = new Scope(); //inicijalizacija pocetnog (globalnog) djelokruga
	public static Stack<Object> stack = new Stack<Object>();
	public static Stack<Object> funcParamsStack = new Stack<Object>();
	public static Stack<Object> inheritanceStack = new Stack<Object>();
	public static Stack<Object> functionCallStack = new Stack<Object>(); //jako glupo ovo sa stackovima...
	
	public static List<VarFRISC> varsFRISC = new ArrayList<VarFRISC>();
	public static Map<String, FunctionFRISC> functionsFRISC = new HashMap<String, FunctionFRISC>();
	public static StringBuilder globalArrays = new StringBuilder();
	public static FunctionFRISC curFunction = null;
	public static StringBuilder codeFRISC = new StringBuilder();
	
	//Ovdje pocinju metode
	
	/**
	 * Metoda za ispis greske!
	 * Znaci kad u metodi nekoj zakljucite da je doslo do greske pozovete ovu
	 * metodu i predate joj node koji ste dobili kao parametar u toj metodi u kojoj se nalazite!
	 * @author wex
	 * @param node
	 */
	public static void printErr(TreeNode node){
		StringBuilder builder = new StringBuilder();
		
		builder.append(node.toString() + " ::=");
		
		for(int i = 0; i < node.getChildrenAmount(); i++)
			builder.append(" " + node.getChild(i).toString());
		
		System.out.println(builder.toString());
		System.exit(0);
	}
	
	
	/**
	 * Ulazna metoda u analizu, kao parametar joj se predaje korijen generativnog stabla
	 * Ova metoda je redundantna i bit ce promijenjena, al zasad nek bude tu cisto da vidimo
	 * koje metode za analizu su implementirane a koje ne.
	 * @author wex
	 * @param node
	 */
	public static void start(TreeNode node){
		if(node.getName().equals("<prijevodna_jedinica>"))
			Command.prijevodna_jedinica(node);
		else{
			System.out.println("Pocetni nezavrsni znak nije <prijevodna_jedinica>!");
			System.exit(1);
		}
	}

}
