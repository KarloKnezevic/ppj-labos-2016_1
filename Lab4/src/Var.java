import java.util.ArrayList;
import java.util.List;


/**
 * UPGRADE: i varijable i funkcije!
 * @author wex
 *
 */
public class Var{
		private String name; //identifikator ove varijable (npr x, y, sum...)
		private boolean l_type; //jel je l-type ili ne (tj jel moze biti s lijeve strane naredbe pridruzivanja
		private String type; //tip ove varijable.. konzultiraj upute za detaljnije informacije...ili povratni tip funkcije
		
		private boolean isFunction;
		private List<String> params; //za funkcije
		private boolean defined;
		
		public Var(String name){
			this.name = name;
		}
		
		/**
		 * konstruktor za varijable
		 * @param name
		 * @param l_type
		 * @param type
		 */
		public Var(String name, boolean l_type, String type){
			this.name = name;
			this.l_type = l_type;
			this.type = type;
			isFunction = false;
			params = null;
			defined = false;
		}
		
		/**
		 * konstruktor za funkcije
		 * @param name
		 * @param params
		 * @param type
		 * @param defined
		 */
		public Var(String name, List<String> params, String type, boolean defined){
			this.name = name;
			l_type = false;
			isFunction = true;
			this.type = type;
			this.params = params;
			this.defined = defined;
		}
		
		public boolean checkFunctionTypes(List<String> params, String retType){
			if(!retType.equals(type))
				return false;
			
			if(!isFunction)
				return false;
			
			if(params.size() != this.params.size())
				return false;
			
			for(int i = 0; i < params.size(); i++)
				if(!params.get(i).equals(this.params.get(i)))
					return false;
			return true;
		}
		
		public void setFunctionDefined(){
			defined = true;
		}
		
		public String getName(){
			return name;
		}
		
		public boolean isL_type(){
			return l_type;
		}
		
		public String getType(){
			return type;
		}
		
		public List<String> getParams(){
			return params;
		}
		
		public boolean isDefined(){
			return defined;
		}
		
		public boolean isFunction(){
			return isFunction;
		}
	}