import java.text.BreakIterator;
import java.util.List;


/**
 * Metode za analizu naredbi (poglavlje 4.4.5. u uputama)
 * @author wex
 *
 */
public abstract class Command {
	
	public static boolean checkIfThisFunctionType(TreeNode node, String type){
		if(!node.getName().equals("<definicija_funkcije>")){
			if(node.getParent() == null)
				return false;
			else
				return checkIfThisFunctionType(node.getParent(), type);
		}
		else{
			if(!node.getChild(0).getName().equals("<ime_tipa>"))
				return false;
			else
				node = node.getChild(0);
			
			if(!node.getChild(0).getName().equals("<specifikator_tipa>"))
				return false;
			else
				node = node.getChild(0).getChild(0);
			
			if(node.getName().equals("KR_VOID") && type.equals("void"))
				return true;
			else if(Expression.isImplicitlyCastable(type, node.getLex_atom()))
				return true;
		}
		return false;
	}
	
	
	/**
	 * @author wex
	 * @param currentNode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean slozena_naredba(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("L_VIT_ZAGRADA") 
			&& node.getChild(1).getName().equals("<lista_naredbi>")){
			Command.lista_naredbi(node.getChild(1));
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("L_VIT_ZAGRADA")
				&& node.getChild(1).getName().equals("<lista_deklaracija>")){
			Declaration.lista_deklaracija(node.getChild(1));
			Command.lista_naredbi(node.getChild(2));
		}
		return true;
	}

	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean lista_naredbi(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("<naredba>")){
			Command.naredba(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("<lista_naredbi>")){
			lista_naredbi(node.getChild(0));
			naredba(node.getChild(1));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean naredba(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("<slozena_naredba>")){
			slozena_naredba(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("<izraz_naredba>")){
			izraz_naredba(node.getChild(0));
		}
		
		// 3. produkcija
		else if(node.getChild(0).getName().equals("<naredba_grananja>")){
			naredba_grananja(node.getChild(0));
		}
		
		// 4. produkcija
		else if(node.getChild(0).getName().equals("<naredba_petlje>")){
			naredba_petlje(node.getChild(0));
		}
		
		// 5. produkcija
		else if(node.getChild(0).getName().equals("<naredba_skoka>")){
			Command.naredba_skoka(node.getChild(0));
		}
		
		return true;
	}
	
	/**
	 * Ova analiza ce staviti na Analyzator.stack svoje svojstvo "tip" (String)
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean izraz_naredba(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("TOCKAZAREZ")){
			// I AIN'T GONNA DO NUTHIN'!
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("<izraz>")){
			Expression.izraz(node.getChild(0));
			AsGen.addAssembler(AsGen.add("R7", "4", "R7"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean naredba_grananja(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 5){
			Expression.izraz(node.getChild(2));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.cmp("R0", "0"));
			String jmpLabel = AsGen.getJumpLabel();
			AsGen.addAssembler(AsGen.jp("EQ", jmpLabel));
			naredba(node.getChild(4));
			AsGen.addAssembler(jmpLabel);
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 7){
			String labelEndIf = AsGen.getJumpLabel();
			String labelElse = AsGen.getJumpLabel();
			Expression.izraz(node.getChild(2));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.cmp("R0", "0"));
			AsGen.addAssembler(AsGen.jp("EQ", labelElse));
			naredba(node.getChild(4));
			AsGen.addAssembler(AsGen.jp(null, labelEndIf));
			AsGen.addAssembler(labelElse);
			naredba(node.getChild(6));
			AsGen.addAssembler(labelEndIf);
		}
		
		return true;
	}

	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean naredba_petlje(TreeNode node){
		Integer loopID = AsGen.getNewLoopID();
		String breakLabel = "LOOP_BREAK_" + loopID.toString() + "\n";
		String continueLabel = "LOOP_CONTINUE_" + loopID.toString() + "\n";
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("KR_WHILE")){
			AsGen.addAssembler(continueLabel);
			Expression.izraz(node.getChild(2));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.cmp("R0", "0"));
			AsGen.addAssembler(AsGen.jp("EQ", breakLabel));
			naredba(node.getChild(4));
			AsGen.addAssembler(AsGen.jp(null, continueLabel));
			AsGen.addAssembler(breakLabel);
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("KR_FOR") && node.getChildrenAmount() == 6){
			izraz_naredba(node.getChild(2));
			AsGen.addAssembler(continueLabel);
			Expression.izraz(node.getChild(3).getChild(0)); //PRESKACEM <izraz_naredba> jer mi treba ono sto ce biti na stogu
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.cmp("R0", "0"));
			AsGen.addAssembler(AsGen.jp("EQ", breakLabel));
			naredba(node.getChild(5));
			AsGen.addAssembler(AsGen.jp(null, continueLabel));
			AsGen.addAssembler(breakLabel);
		}
		
		// 3. produkcija
		else if(node.getChild(0).getName().equals("KR_FOR") && node.getChildrenAmount() == 7){
			izraz_naredba(node.getChild(2));
			String loopBackLabel = AsGen.getJumpLabel();
			AsGen.addAssembler(loopBackLabel);
			Expression.izraz(node.getChild(3).getChild(0)); //PRESKACEM <izraz_naredba> jer mi treba ono sto ce biti na stogu
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.cmp("R0", "0"));
			AsGen.addAssembler(AsGen.jp("EQ", breakLabel));
			naredba(node.getChild(6));
			AsGen.addAssembler(continueLabel);
			Expression.izraz(node.getChild(4));
			AsGen.addAssembler(AsGen.add("R7", "4", "R7"));
			AsGen.addAssembler(AsGen.jp(null, loopBackLabel));
			AsGen.addAssembler(breakLabel);
		}
		
		AsGen.leaveLoop();
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean naredba_skoka(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("KR_CONTINUE")){
			AsGen.addAssembler(AsGen.jp(null, "LOOP_CONTINUE_" + Integer.valueOf(AsGen.peekLoopID()).toString()));
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("KR_BREAK")){
			AsGen.addAssembler(AsGen.jp(null, "LOOP_BREAK_" + Integer.valueOf(AsGen.peekLoopID()).toString()));
		}
		
		// 3. produkcija
		else if(node.getChild(0).getName().equals("KR_RETURN") && node.getChildrenAmount() == 2){
			AsGen.addAssembler(AsGen.jp(null, Analyzator.curFunction.funcName + "_END"));
		}
		
		// 4. produkcija
		else if(node.getChild(0).getName().equals("KR_RETURN") && node.getChildrenAmount() == 3){
			Expression.izraz(node.getChild(1));
			AsGen.addAssembler(AsGen.pop("R0"));
			AsGen.addAssembler(AsGen.move("R0", "R6"));
			AsGen.addAssembler(AsGen.jp(null, Analyzator.curFunction.funcName + "_END"));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean prijevodna_jedinica(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			vanjska_deklaracija(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 2){
			prijevodna_jedinica(node.getChild(0));
			vanjska_deklaracija(node.getChild(1));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean vanjska_deklaracija(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("<definicija_funkcije>")){
			Declaration.definicija_funkcije(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("<deklaracija>")){
			Declaration.deklaracija(node.getChild(0));
		}
		
		return true;
	}

}
