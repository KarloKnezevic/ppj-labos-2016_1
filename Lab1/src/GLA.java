import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GLA {




/**
 * Epsilon NKA, klasa sadrzi metode za dodavanje prijelaza (trenutno_stanje,
 * ulazni_znak -> novi_skup_stanja) za prijelaze je napravljena klasa Prijelaz
 * unutar klase E_NKA za laksi pristup komponentama prijelaza (trenutno_stanje
 * itd.)
 * 
 * @author wex
 *
 */
public static class E_NKA {

	private String regex; // regex koji ovaj automat prihvaca IME REGEX-A!!
	private Set<Prijelaz> prijelazi;
	private String poc_stanje;
	private String[] prihvatljiva_stanja;
	private Set<String> trenutna_stanja;
	private int br_stanja;

	/**
	 * Konstruktor za enka, parametar "prihvatljiva_stanja" mora biti u obliku
	 * "stanje1,stanje2,stanje3,stanje4" itd
	 * 
	 * @param poc_stanje
	 * @param prihvatljiva_stanja
	 */
	public E_NKA(String regex, String poc_stanje, String prihvatljiva_stanja) {
		this.regex = regex;
		this.trenutna_stanja = new LinkedHashSet<String>();
		prijelazi = new LinkedHashSet<Prijelaz>();
		this.poc_stanje = poc_stanje;
		this.prihvatljiva_stanja = prihvatljiva_stanja.split(",");
		this.br_stanja = 0;

	}

	/**
	 * Novi skup stanja mora biti String oblika
	 * "stanje1,stanje2,stanje3,stanje4" itd
	 * 
	 * @param trenutno
	 *            stanje
	 * @param ulazni
	 *            znak
	 * @param novi
	 *            skup stanja
	 */
	public void addPrijelaz(String ts, char uz, String nss) {
		prijelazi.add(new Prijelaz(ts, uz, nss, false));
	}
	
	public void addEPrijelaz(String ts, String nss){
		prijelazi.add(new Prijelaz(ts, '$', nss, true));
	}

	/**
	 * Getter za regex koji ovaj automat prihvaca.
	 * 
	 * @return
	 */
	public String getRegex() {
		return regex;
	}

	/**
	 * Metoda inicira enka na epsilon-okruzje pocetnog stanja enka (logicno,
	 * sluzi i resetiranju enka) STARA METODA NE KORISTI SE!
	 */
	/*
	 * public void init(){ trenutna_stanja.clear();
	 * trenutna_stanja.add(poc_stanje);
	 * 
	 * this.doEpsilonPrijelaz(); }
	 */

	/**
	 * Metoda ce napraviti prijelaz za
	 * ovaj enka na temelju njegovih trenutnih stanja i predanog parametra koji
	 * predstavlja ulazni znak.
	 * 
	 * @param ulazni znak
	 * @return prihvatljivo stanje (true) ili ne (false)
	 */
	public void radiPrijelaz(char uz) {
		Set<String> nova_stanja = new LinkedHashSet<String>();
		
		for(String stanje : trenutna_stanja){
			for(Prijelaz prijelaz : prijelazi){
				if(prijelaz.ts.equals(stanje) && prijelaz.uz == uz && !prijelaz.jeEpsilonPrijelaz){
					for(String novo_stanje : prijelaz.nss){
						nova_stanja.add(novo_stanje);
					}
				}
			}
		}
		trenutna_stanja = nova_stanja;
		doEpsilonPrijelaz();
	}

	/**
	 * Metoda ce napraviti epsilon prijelaze za ovaj enka. Metoda vraca 'true'
	 * ako je enka u prihvatljivom stanju nakon izvodenja prijelaza, u suprotnom
	 * vraca 'false'
	 * 
	 * @return prihvatljivo stanje (true) ili ne (false)
	 */
	public boolean doEpsilonPrijelaz() {
		boolean changed = true;
		Set<String> novaStanja = new LinkedHashSet<String>();

		/*
		 * da znam da bi se ovaj dio pod ovom "while" petljom mogao puno
		 * efikasnije rijesiti al stari, onak, ne da mi se
		 */

		while (changed) {
			novaStanja = new LinkedHashSet<String>();
			novaStanja.addAll(trenutna_stanja);
			changed = false;
			for (String stanje : trenutna_stanja) {
				for (Prijelaz prijelaz : prijelazi) {
					if (prijelaz.ts.equals(stanje) && prijelaz.jeEpsilonPrijelaz) {
						novaStanja.addAll(Arrays.asList(prijelaz.nss));
					}
				}
			}
			changed = trenutna_stanja.addAll(novaStanja);
		}

		for (String stanje : trenutna_stanja) {
			if (Arrays.asList(prihvatljiva_stanja).contains(stanje))
				return true;
		}
		return false;
	}

	/**
	 * Nakon sto se kreira automat iz regexa, on ima jedno pocetno i jedno
	 * privatljivo stanje Da se ne zezamo sa dva poziva metoda dodaj stanje,
	 * objedinjeno je u jednu.
	 * 
	 * @param lijevo
	 * @param desno
	 */
	public void dodajPocetnoIPrihvatiljivoStanje(String lijevo, String desno) {
		this.poc_stanje = lijevo;
		this.trenutna_stanja.add(lijevo);
		this.prihvatljiva_stanja = new String[1];
		this.prihvatljiva_stanja[0] = desno;

	}

	/**
	 * Indeksira stanja prilikom kreiranja automata.
	 * 
	 * @return
	 */
	public String novo_stanje() {
		this.br_stanja = this.br_stanja + 1;
		return Integer.toString(br_stanja - 1);
	}

	public String toString() {
		String returnValue = "";
		for (Prijelaz pr : prijelazi) {
			returnValue += pr.toString() + "&&";
		}
		return returnValue.substring(0, returnValue.length() - 2);
	}
	
	public void init(){
		trenutna_stanja.clear();
		trenutna_stanja.add("0");
		doEpsilonPrijelaz();
	}
	
	public boolean stanjaPrazna(){
		return trenutna_stanja.isEmpty();
	}
	
	public boolean uPrihvatljivom(){
		return trenutna_stanja.contains("1");
	}

	private class Prijelaz {
		private String ts; // Trenutno Stanje
		private char uz; // Ulazni Znak
		private String[] nss; // Novi Skup Stanja
		private boolean jeEpsilonPrijelaz;

		/**
		 * Novi skup stanja mora biti String oblika
		 * "stanje1,stanje2,stanje3,stanje4" itd
		 * 
		 * @param trenutno
		 *            stanje
		 * @param ulazni
		 *            znak
		 * @param novi
		 *            skup stanja
		 */
		public Prijelaz(String ts, char uz, String nss, boolean isEpsilon) {
			this.ts = ts;
			this.uz = uz;
			this.nss = nss.split(",");
			this.jeEpsilonPrijelaz = isEpsilon;
		}

		/**
		 * ts,,uz->[nss] Za stanje ts, uz procitani znak uz, prijedji u skup
		 * stanja nss
		 */
		public String toString() {
			String ispis = "";
			ispis += this.ts + ",," + Character.toString(uz) + "->" + Arrays.toString(nss);
			if(jeEpsilonPrijelaz)
				ispis += "D";
			else
				ispis += "N";
			return ispis;
		}

		public boolean equals(Object other) {
			Prijelaz o = (Prijelaz) other;
			return o.toString().equals(this.toString());
		}
	}

}

	
	




/**
 * Klasa za pravila leksickog analizatora, sadrzi atribute za spremanje u kojem
 * stanju LA mora biti da bi pravilo bilo aktivno, navedeni regex koji se mora
 * detektirati (jos uvijek nisam siguran kako izvesti ono ako nije regex koji je
 * prije definiran) dalje sprema
 * 
 * @author wex
 *
 */
public static class Pravilo {
	private String stanjeLA;
	private E_NKA enka;
	private String lex_jedinka;
	private List<String> posebni_argumenti;
	private List<String> svi_argumenti;

	/**
	 * Prvi parametar je stanjeLA u kojem treba biti LA da bi ovo pravilo bilo
	 * aktivno.
	 * 
	 * Drugi parametar je automat koji provjerava jel trenutni ulazni niz
	 * regularni izraz kojeg zahtjeva ovo pravilo.
	 * 
	 * Treci parametar su svi argumenti u tijelu pravila spremljeni u listu
	 * stringova, konstruktor ce uzeti prvi (indeks 0) element s liste i
	 * spremiti ga kao prepoznatu leksicku jedinku (procitajte format tih
	 * pravila u upute.pdf), ostali elementi s liste se spremaju kao posebni
	 * argumenti (NOVI_RED etc...)
	 * 
	 * @param stanjeLA
	 * @param regex
	 * @param argumenti
	 */
	public Pravilo(String stanjeLA, E_NKA enka, List<String> argumenti) {
		this.stanjeLA = stanjeLA;
		this.enka = enka;
		this.lex_jedinka = argumenti.get(0);
		this.svi_argumenti = new ArrayList<String>();
		this.svi_argumenti.addAll(argumenti);
		argumenti.remove(0);
		if (argumenti.size() == 0)
			posebni_argumenti = null;
		else
			this.posebni_argumenti = argumenti;
	}

	/**
	 * Getter za podatak u kojem stanju treba biti LA da bi ovo pravilo bilo
	 * aktivno.
	 * 
	 * @return
	 */
	public String getStanjeLA() {
		return this.stanjeLA;
	}

	public String toString() {
		String args = "";
		for (String arg : svi_argumenti) {
			args += arg + "##";
		}
		String aaa = this.stanjeLA + "%%" + this.enka.toString() + "%%" + args.substring(0, args.length() - 2);
		return aaa;
	}
	
	public void inicirajAutomat(){
		enka.init();
	}
	
	public boolean enkaStanjaPrazna(){
		return enka.stanjaPrazna();
	}
	
	public boolean enkaUPrihvatljivom(){
		return enka.uPrihvatljivom();
	}
	
	public void enkaRadiPrijelaz(char uz){
		enka.radiPrijelaz(uz);
	}

}

	
	
	
	
	
	
	
	
	
	/*
	 * Ispisivanje pravila u datoteku za prosljedjivanje LA Format:
	 * <imeStanja>%%prijelazi automata odvojeni s dva znaka & u formatu
	 * ts,,uz->[nss]E%%argumenti odvojeni s dva znaka # primjer:
	 * <S_pocetno>%%2,,]->[3]N&&0,,$->[2]D&&3,,$->[1]D%%D_UGL_ZAGRADA
	 * ovaj 'D' ili 'N' predstavlja jel se radi o epsilon prijelazu ili ne
	 */

	public static void main(String[] args) throws Exception {

		// Daljnji kod GLA
		Map<String, RegularnaDefinicija> regularneDefinicije = new LinkedHashMap<String, RegularnaDefinicija>();
		List<String> stanjaLA = new ArrayList<String>();
		List<String> imenaLexJedinki = new ArrayList<String>();
		List<Pravilo> pravila = new ArrayList<Pravilo>();

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();

		// obrada regularnih definicija
		while (!line.startsWith("%X")) {
			RegularnaDefinicija def = obradaRegularneDefinicije(line, regularneDefinicije);
			regularneDefinicije.put(def.name, def);
			line = reader.readLine();
		}

		// obrada %X stanja leksickog analizatora
		line = line.substring(3);
		String[] split = line.split(" ");
		for (String part : split) {
			stanjaLA.add(part);
		}
		line = reader.readLine();

		// obrada %L stanja leksickih jedinki
		line = line.substring(3);
		split = line.split(" ");
		for (String part : split) {
			imenaLexJedinki.add(part);
		}
		line = reader.readLine();

		// obrada pravila leksickog analizatora
		while (line != null && !line.equals("")) {
			String imeStanjaPravila = "";
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < line.length(); i++) {
				sb.append(line.charAt(i));
				if (line.charAt(i) == '>') {
					imeStanjaPravila = sb.toString();
					break;
				}

			}
			// trazi referencirane definicije
			String rest = line.replaceAll(imeStanjaPravila, "");

			String regexPravila = (obradaRegularneDefinicije(" " + rest, regularneDefinicije)).regex;

			E_NKA automatPravila = new E_NKA(regexPravila, "", "");
			ParStanja pocetnoIPrihvatljivoPravila = pretvori(regexPravila, automatPravila);
			automatPravila.dodajPocetnoIPrihvatiljivoStanje(pocetnoIPrihvatljivoPravila.lijevo_stanje, pocetnoIPrihvatljivoPravila.desno_stanje);

			List<String> argumentiAkcije = new ArrayList<String>();
			line = reader.readLine();
			line = reader.readLine();
			while (!line.equals("}")) {
				argumentiAkcije.add(line);
				line = reader.readLine();
			}
			line = reader.readLine();
			pravila.add(new Pravilo(imeStanjaPravila, automatPravila, argumentiAkcije));

		}

		reader.close();

		/*
		 * Ispisivanje pravila u datoteku za prosljedjivanje LA Format:
		 * <imeStanja>%%prijelazi automata odvojeni s dva znaka & u formatu
		 * ts,,uz->[nss]%%argumenti odvojeni s dva znaka # primjer:
		 * <ime>%%a,,b->[a,b]&&a,,c->[b,c]%%-##VRATI_SE 0&#&#
		 */
		PrintWriter izlaz = new PrintWriter("./analizator/izlaz.txt");
		izlaz.println(stanjaLA.get(0));
		for (Pravilo pravilo : pravila) {
			izlaz.print(pravilo.toString() + "&#&#");
		}
		izlaz.close();
	}

	/**
	 * Metoda koja obraduje red koji je prepoznat kao regularna definicija.
	 * 
	 * @param line
	 *            je ucitani red.
	 * @return vraca novi objekt RegularnaDefinicija.
	 */
	private static RegularnaDefinicija obradaRegularneDefinicije(String line, Map<String, RegularnaDefinicija> regdef) {
		int splitPos = line.indexOf(" ");
		String name = line.substring(0, splitPos);
		String rest = line.substring(splitPos + 1);
		char[] restChar = rest.toCharArray();
		Set<String> referencedDef = new LinkedHashSet<String>();

		// trazi referencirane definicije iskoristen kod
		for (int i = 0; i < restChar.length; i++) {
			if (restChar[i] == '{' && je_operator(restChar, i)) {
				StringBuilder sb = new StringBuilder();
				for (int j = i; j < restChar.length; j++) {
					if (restChar[j] != '}') {
						sb.append(restChar[j]);
					} else {
						sb.append(restChar[j]);
						referencedDef.add(sb.toString());
						break;
					}
				}
			}
		}
		// zamjenjuje referencirane definicije sa njihovim regexom unutar
		// zagrada
		for (String ref : referencedDef)
			rest = rest.replace(ref, "(" + regdef.get(ref).regex + ")");

		return new RegularnaDefinicija(name, rest);
	}

	/**
	 * Klasa za modeliranje regularne definicije. Odabrao sam to jer mislim da
	 * je ipak lakse nego igrati se kolekcijama.
	 *
	 */

	private static class RegularnaDefinicija {
		private String name;
		private String regex;

		public RegularnaDefinicija(String name, String regex) {
			this.name = name;
			this.regex = regex;
		}

		public String getName() {
			return name;
		}

		public String getExpressions() {
			return regex;
		}

		public boolean equals(Object o) {
			RegularnaDefinicija other = (RegularnaDefinicija) o;
			return this.name.equals(other.name);
		}

		public String toString() {
			return this.name + " " + regex;
		}
	}

	public static boolean je_operator(char[] izraz, int index) {
		int i = index;
		int br = 0;
		while (i - 1 >= 0 && izraz[i - 1] == '\\') { // ovo je jedan \, kao u Cu
			br = br + 1;
			i = i - 1;
		}
		return br % 2 == 0;

	}

	public static ParStanja pretvori(String regularni_izraz, E_NKA automat) {
		List<String> izbori = new ArrayList<String>();

		int br_zagrada = 0;
		char[] izraz = regularni_izraz.toCharArray();
		boolean operatorIzrazaPronadjen = false;
		int indexZadnjegOperatora = 0;

		for (int i = 0; i < izraz.length; i++) {

			if (izraz[i] == '(' && je_operator(izraz, i)) {
				br_zagrada++;
			} else if (izraz[i] == ')' && je_operator(izraz, i)) {
				br_zagrada--;
			} else if (br_zagrada == 0 && izraz[i] == '|' && je_operator(izraz, i)) {
				StringBuilder builder = new StringBuilder();
				for (int j = indexZadnjegOperatora; j < i; j++) {
					builder.append(izraz[j]);
				}
				izbori.add(builder.toString());
				operatorIzrazaPronadjen = true;
				indexZadnjegOperatora = i + 1;
			}
		}

		if (operatorIzrazaPronadjen == true) {
			StringBuilder builder = new StringBuilder();
			for (int j = indexZadnjegOperatora; j < izraz.length; j++) {
				builder.append(izraz[j]);
			}
			izbori.add(builder.toString());
			operatorIzrazaPronadjen = true;
		}

		String lijevo_stanje = automat.novo_stanje();
		String desno_stanje = automat.novo_stanje();
		if (operatorIzrazaPronadjen) {
			for (int i = 0; i < izbori.size(); i++) {
				ParStanja privremeno = pretvori(izbori.get(i), automat);
				automat.addEPrijelaz(lijevo_stanje, privremeno.lijevo_stanje);
				automat.addEPrijelaz(privremeno.desno_stanje, desno_stanje);
			}
		} else {
			boolean prefiksirano = false;
			String zadnje_stanje = lijevo_stanje;
			for (int i = 0; i < izraz.length; i++) {
				String a, b;
				if (prefiksirano) {
					prefiksirano = false;
					char prijelazni_znak;
					if (izraz[i] == 't')
						prijelazni_znak = '\t';
					else if (izraz[i] == 'n')
						prijelazni_znak = '\n';
					else if (izraz[i] == '_')
						prijelazni_znak = ' ';
					else
						prijelazni_znak = izraz[i];

					a = automat.novo_stanje();
					b = automat.novo_stanje();
					automat.addPrijelaz(a, prijelazni_znak, b);
				} else {
					if (izraz[i] == '\\') {
						prefiksirano = true;
						continue;
					}

					if (izraz[i] != '(') {
						a = automat.novo_stanje();
						b = automat.novo_stanje();
						if (izraz[i] == '$') {
							automat.addEPrijelaz(a, b);
						} else {
							automat.addPrijelaz(a, izraz[i], b);
						}
					} else {
						int j = 0;
						int brojOtvorenihZagrada = 0;
						for (int k = i; k < izraz.length; k++) {
							if (izraz[k] == '(') {
								brojOtvorenihZagrada++;
							} else if (izraz[k] == ')') {
								brojOtvorenihZagrada--;
							}
							if (brojOtvorenihZagrada == 0) {
								j = k;
								break;
							}
						}

						StringBuilder izrazOdip1dojm1 = new StringBuilder();
						for (int k = i + 1; k < j; k++) {
							izrazOdip1dojm1.append(izraz[k]);
						}
						ParStanja privremeno = pretvori(izrazOdip1dojm1.toString(), automat);
						a = privremeno.lijevo_stanje;
						b = privremeno.desno_stanje;
						i = j;
					}
				}
				if ((i + 1) < izraz.length && izraz[i + 1] == '*') {
					String x = a;
					String y = b;
					a = automat.novo_stanje();
					b = automat.novo_stanje();
					automat.addEPrijelaz(a, x);
					automat.addEPrijelaz(y, b);
					automat.addEPrijelaz(a, b);
					automat.addEPrijelaz(y, x);
					i++;
				}

				automat.addEPrijelaz(zadnje_stanje, a);
				zadnje_stanje = b;
			}
			automat.addEPrijelaz(zadnje_stanje, desno_stanje);
		}
		return new ParStanja(lijevo_stanje, desno_stanje);
	}

	private static class ParStanja {
		public String lijevo_stanje;
		public String desno_stanje;

		public ParStanja(String lijevo_stanje, String desno_stanje) {
			this.lijevo_stanje = lijevo_stanje;
			this.desno_stanje = desno_stanje;
		}

		public String toString() {
			return this.lijevo_stanje + ", " + this.desno_stanje;
		}

		public boolean equals(Object other) {
			ParStanja o = (ParStanja) other;
			return this.toString().equals(o.toString());
		}
	}

}