import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Ova klasa zapravo nicem ne sluzi ali kod je mnogo pregledniji i jasniji.
 * 
 * @author luka
 *
 */

public class Gramatika {
	public List<String> zavrsni;
	public List<String> nezavrsni;
	public List<Produkcija> produkcije;
	public List<String> svi;
	public Set<String> zav;
	public Set<String> nezav;

	public Gramatika(List<String> zavrsni, List<String> nezavrsni, List<Produkcija> produkcije) {
		this.zavrsni = zavrsni;
		this.nezavrsni = nezavrsni;
		this.produkcije = produkcije;
		this.zav = new HashSet<String>(zavrsni);
		zav.add("\27");
		this.nezav = new HashSet<String>(nezavrsni);
		this.svi = new ArrayList<String>();
		this.svi.addAll(nezavrsni);
		this.svi.addAll(zavrsni);
	}
}
