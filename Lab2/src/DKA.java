import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author luka
 *
 */

public class DKA {

	public Set<DKAStanje> stanja = new LinkedHashSet<DKAStanje>();
	public Map<DKAStanje, Integer> stanjaSIndexima = new LinkedHashMap<DKAStanje, Integer>();

	public Map<LRStavka, Map<String, List<LRStavka>>> mapaPrijelaza;

	public Set<Prijelaz> prijelazi = new LinkedHashSet<DKA.Prijelaz>();

	public DKAStanje pocetno;

	public DKA(ENKA enka) {

		// index stanja
		int index = 0;

		this.mapaPrijelaza = enka.mapaPrijelaza;
		DKAStanje stanje0 = new DKAStanje(nadjiEpsilonOkruzenje(enka.pocetnoStanje), index++);
		stanja.add(stanje0);

		pocetno = stanje0;

		List<String> znakovi = enka.gramatika.svi;
		znakovi.remove("<%>");

		boolean bilaPromjena = true;

		Set<DKAStanje> stanjaProsleIteracije = new LinkedHashSet<DKAStanje>();
		stanjaProsleIteracije.add(pocetno);

		while (bilaPromjena) {
			// drzi nova stanja
			Set<DKAStanje> novaPronadjenaStanja = new LinkedHashSet<DKAStanje>();
			for (DKAStanje stanje : stanjaProsleIteracije) {
				// za svaku LRStavku i svaki znak se prelazi u neko stanje
				Set<LRStavka> elementi = stanje.elementi;
				for (String znak : znakovi) {
					// stanja u koja se prelazi iz DKA stanja za znak
					Set<LRStavka> prijedjena = new LinkedHashSet<LRStavka>();
					for (LRStavka element : elementi) {
						Map<String, List<LRStavka>> stavke = mapaPrijelaza.get(element);
						if (stavke != null) {
							try {
								prijedjena.addAll(stavke.get(znak));
							} catch (Exception e) {

							}
						}

					}

					// ako se ne prelazi u nista nista ne treba ni dodati
					if (!prijedjena.isEmpty()) {
						Set<LRStavka> eps = new LinkedHashSet<LRStavka>();
						for (LRStavka novo : prijedjena) {
							eps.addAll(nadjiEpsilonOkruzenje(novo));
						}

						prijedjena.addAll(eps);

						DKAStanje novoStanje = new DKAStanje(new LinkedHashSet<LRStavka>(prijedjena), index++);
						novaPronadjenaStanja.add(novoStanje);
						prijelazi.add(new Prijelaz(stanje, znak, novoStanje));
					}

				}
			}
			// da ne ponavljas pretragu po stanjima koja si vec prosao
			stanjaProsleIteracije = novaPronadjenaStanja;
			stanjaProsleIteracije.removeAll(stanja);
			
			bilaPromjena = stanja.addAll(stanjaProsleIteracije);
			
			

		}

		// postavljamo indexe stanja
		this.dodajIndexe();
	}

	public void dodajIndexe() {
		int i = 0;
		for (DKAStanje stanje : stanja) {
			stanje.index = i;
			stanjaSIndexima.put(stanje, stanje.index);
			i++;
		}

		for (Prijelaz prijelaz : prijelazi) {
			prijelaz.pocetno.index = stanjaSIndexima.get(prijelaz.pocetno);
			prijelaz.konacno.index = stanjaSIndexima.get(prijelaz.konacno);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (DKAStanje stanje : stanja) {
			sb.append(stanje.index + ")" + stanje.elementi + "\n");
		}

		for (Prijelaz prijelaz : prijelazi) {
			sb.append(prijelaz.pocetno + "***ZNAK: " + prijelaz.znak + "  ***NOVO:***" + prijelaz.konacno + "\n");
		}

		return sb.toString();
	}

	private Set<LRStavka> nadjiEpsilonOkruzenje(LRStavka lrStavka) {
		Set<LRStavka> novaStanja = new LinkedHashSet<LRStavka>();
		novaStanja.add(lrStavka);
		boolean bilaPromjena = true;
		while (bilaPromjena) {
			Set<LRStavka> novaPronadjenaStanja = new LinkedHashSet<LRStavka>();
			for (LRStavka stavka : novaStanja) {
				Map<String, List<LRStavka>> st = mapaPrijelaza.get(stavka);
				if (st != null) {
					try {
						novaPronadjenaStanja.addAll(st.get(""));
					} catch (Exception e) {

					}
				}
			}

			bilaPromjena = novaStanja.addAll(novaPronadjenaStanja);
		}

		return novaStanja;
	}

	public class Prijelaz {
		public DKAStanje pocetno;
		public String znak;
		public DKAStanje konacno;

		public Prijelaz(DKAStanje pocetno, String znak, DKAStanje konacno) {
			this.pocetno = pocetno;
			this.znak = znak;
			this.konacno = konacno;
		}

		public String toString() {
			return this.pocetno.toString() + "; " + znak + " -> " + this.konacno.toString();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((konacno == null) ? 0 : konacno.hashCode());
			result = prime * result + ((pocetno == null) ? 0 : pocetno.hashCode());
			result = prime * result + ((znak == null) ? 0 : znak.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Prijelaz other = (Prijelaz) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (konacno == null) {
				if (other.konacno != null)
					return false;
			} else if (!konacno.equals(other.konacno))
				return false;
			if (pocetno == null) {
				if (other.pocetno != null)
					return false;
			} else if (!pocetno.equals(other.pocetno))
				return false;
			if (znak == null) {
				if (other.znak != null)
					return false;
			} else if (!znak.equals(other.znak))
				return false;
			return true;
		}

		private DKA getOuterType() {
			return DKA.this;
		}
	}
}
