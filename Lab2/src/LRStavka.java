import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * OVA KLASA NUDI SLJEDECE MOGUCNOSTI ---- VRATI LR STAVKU S TOCKOM POMAKNUTO
 * JEDNO MJESTO UDESNO ---- IMA VARIJABLE X I BETA. X JE ZNAK NAKON TOCKE, BETA
 * JE NIZ NAKON X-A ---- IMA POLJE U KOJE SU SPREMNLJENI SVI ZNAKOVI DESNE
 * STRANE
 * 
 * @author luka
 *
 */
public class LRStavka {
	public Produkcija produkcija;
	public String lijevaStrana;
	public String desnaStrana;
	public int tockaIndex;
	public Set<String> skupDesneStrane;
	public int brojZnakovaDesneStrane;
	public String[] znakoviDesneStrane;

	// ovo X i beta su oznake kao u udzbeniku u onom algoritmu
	// X je znak nakon tocke
	// beta je niz nakon X-a
	public String beta = "";
	// ovaj X nekad oznacavaju kao B
	public String X;

	public LRStavka(Produkcija prod, int index, Set<String> skupDesneStrane) {
		this.produkcija = prod;
		// ovdje izbacujem sve praznine iz desne strane, tako je lakse raditi
		this.desnaStrana = prod.desnaStrana.replaceAll(" ", "");

		if (this.desnaStrana.equals("$")) {
			this.desnaStrana = "";
		}

		this.lijevaStrana = prod.lijevaStrana;

		// oznacava index tocke, index 0 znaci da je tocka prije svih znakova
		// desne strane, index 1 da je nakon prvog, index 2 da je nakon drugog
		// znaka itd.
		this.tockaIndex = index;

		// za lakse rukovanje stavljam sve znakove u array jedan
		if (prod.desnaStrana.contains(" ")) {
			this.znakoviDesneStrane = prod.desnaStrana.trim().split(" ");
			this.brojZnakovaDesneStrane = this.znakoviDesneStrane.length;
		} else {
			this.znakoviDesneStrane = new String[] { prod.desnaStrana.trim() };
			this.brojZnakovaDesneStrane = 1;
		}

		// traljavo ali izbjegava ArrayIndexOUtOfBound errore
		for (int i = tockaIndex; i < brojZnakovaDesneStrane; i++) {
			this.beta += znakoviDesneStrane[i];
		}

		try {
			this.X = znakoviDesneStrane[tockaIndex];
		} catch (Exception e) {
			this.X = "";
		}
		try {
			this.beta = this.beta.replaceFirst(this.X, "");
		} catch (Exception e) {

		}

		this.skupDesneStrane = skupDesneStrane;
	}

	/**
	 * Tocka je u ispisu oznacena sa ovim znakom: , to nema nikakve koristi osim
	 * za nama lakse debagiranje osim za nama lakse debagiranje
	 */
	public String toString() {
		if (this.lijevaStrana.equals("q0")) {
			return "q0";
		}

		String returnVal = this.lijevaStrana + " ::= ";
		for (int i = 0; i < tockaIndex; i++) {
			returnVal += znakoviDesneStrane[i];
		}

		// returnVal += "";

		for (int i = tockaIndex; i < brojZnakovaDesneStrane; i++) {
			returnVal += znakoviDesneStrane[i];
		}

		returnVal += ", " + this.skupDesneStrane;

		return returnVal;
	}

	/**
	 * Vraca istu LR stavku ali je tocka pomaknuta jedno mjesto udesno Vraca
	 * null ako takva stavka ne postoji.
	 * 
	 * @return
	 */
	public LRStavka istiSTockomUdesno() {
		if ((tockaIndex) < brojZnakovaDesneStrane) {
			return new LRStavka(produkcija, tockaIndex + 1, skupDesneStrane);
		} else {
			return null;
		}

	}

	public List<LRStavka> stanjaSPomaknutomTockom() {
		Set<LRStavka> stanja = new LinkedHashSet<LRStavka>();
		LRStavka novoStanje;

		if (this.desnaStrana.equals("")) {
			return new ArrayList<LRStavka>();
		}

		LRStavka staroStanje = this.istiSTockomUdesno();
		if (staroStanje == null) {
			return new ArrayList<LRStavka>();
		}
		stanja.add(staroStanje);
		while ((novoStanje = staroStanje.istiSTockomUdesno()) != null) {
			stanja.add(novoStanje);
			staroStanje = novoStanje;

		}
		return new ArrayList<LRStavka>(stanja);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((desnaStrana == null) ? 0 : desnaStrana.hashCode());
		result = prime * result
				+ ((lijevaStrana == null) ? 0 : lijevaStrana.hashCode());
		result = prime * result
				+ ((skupDesneStrane == null) ? 0 : skupDesneStrane.hashCode());
		result = prime * result + tockaIndex;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LRStavka other = (LRStavka) obj;
		if (desnaStrana == null) {
			if (other.desnaStrana != null)
				return false;
		} else if (!desnaStrana.equals(other.desnaStrana))
			return false;
		if (lijevaStrana == null) {
			if (other.lijevaStrana != null)
				return false;
		} else if (!lijevaStrana.equals(other.lijevaStrana))
			return false;
		if (skupDesneStrane == null) {
			if (other.skupDesneStrane != null)
				return false;
		} else if (!skupDesneStrane.equals(other.skupDesneStrane))
			return false;
		if (tockaIndex != other.tockaIndex)
			return false;
		return true;
	}
}
