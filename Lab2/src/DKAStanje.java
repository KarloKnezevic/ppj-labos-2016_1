import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 
 * @author luka
 *
 */
public class DKAStanje {
	public Set<LRStavka> elementi = new LinkedHashSet<LRStavka>();
	public int index;

	public DKAStanje(Set<LRStavka> elementi) {
		this.elementi = elementi;
	}

	public DKAStanje(Set<LRStavka> elementi, int index) {
		this.elementi = elementi;
		this.index = index;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((elementi == null) ? 0 : elementi.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DKAStanje other = (DKAStanje) obj;
		if (elementi == null) {
			if (other.elementi != null)
				return false;
		} else if (!elementi.equals(other.elementi))
			return false;
		return true;
	}

	public boolean dodajElement(LRStavka element) {
		return this.elementi.add(element);
	}

	public boolean sadrziElement(LRStavka element) {
		return this.elementi.contains(element);
	}

	public String toString() {
		return this.elementi.toString();
		// return new String(""+index);
	}
}
