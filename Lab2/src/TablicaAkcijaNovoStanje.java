import java.io.PrintWriter;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Tablica sa 150. i 151. stranice u udzbeniku.
 * 
 * @author Murta
 */
public class TablicaAkcijaNovoStanje {

	public Map<Integer, Map<String, Akcija>> akcija = new LinkedHashMap<Integer, Map<String, Akcija>>();
	public Map<Integer, Map<String, Integer>> novoStanje = new LinkedHashMap<Integer, Map<String, Integer>>();
	public Set<Integer> stanja = new HashSet<Integer>();

	public TablicaAkcijaNovoStanje(Gramatika gramatika, DKA dka,
			String[] sinkronizacijskiZnakovi) throws Exception {
		this.pomakniStavi(gramatika, dka);
		this.ReducirajPrihvati(gramatika, dka);
		this.ispisiSe(gramatika, dka, sinkronizacijskiZnakovi);
	}

	/**
	 * Ispis u tekstualnu datoteku. Buduci da si rekao da tablica novoStanje
	 * mora biti puna, ako nema prijelaza pise da se prelazi u -1, stanje koje
	 * ne postoji. FORMAT je: 1. red: pocetno stanje
	 * brojSTanja###ZNAK:znak&&&AKCIJA:akcija...###ZNAK:znak&&&NOVO:novo -svako
	 * stanje je opisano samo u 1 redu FORMAT akcija je identican kao u knjizi.
	 * Pise ti u toString() metodama.
	 * 
	 * @param sinkronizacijskiZnakovi
	 */
	public void ispisiSe(Gramatika gramatika, DKA dka,
			String[] sinkronizacijskiZnakovi) throws Exception {

		PrintWriter izlaz = new PrintWriter("./analizator/izlaz.txt");
		// PrintWriter izlaz = new PrintWriter("D:/izlaz.txt");

		izlaz.println(dka.pocetno.index);
		for (Integer stanje : stanja) {
			StringBuilder sb = new StringBuilder();
			sb.append(stanje + "###");
			Map<String, Akcija> mapaZavrsnih = akcija.get(stanje);
			if (mapaZavrsnih == null) {
				mapaZavrsnih = new LinkedHashMap<String, Akcija>();
			}
			boolean promjena = false;
			for (String znak : gramatika.zav) {
				Akcija a = mapaZavrsnih.get(znak);
				if (a == null) {
					continue;
					// a = new Akcija();
				}
				sb.append(znak + "%%%" + a.toString() + "&&&");
				promjena = true;
				// sb.append("ZNAK:"+znak+"&&&AKCIJA:"+a.toString()+"###");
			}
			if (promjena)
				sb.delete(sb.length() - 3, sb.length() + 1);
			sb.append("###");
			Map<String, Integer> mapaNezz = novoStanje.get(stanje);
			if (mapaNezz == null) {
				mapaNezz = new LinkedHashMap<String, Integer>();
			}
			promjena = false;
			for (String znak : gramatika.nezav) {
				Integer novoStanje = mapaNezz.get(znak);
				if (novoStanje == null) {
					continue;
					// novoStanje = -1;
				}
				// sb.append("ZNAK:"+znak+"&&&NOVO:"+novoStanje.toString()+"###");
				sb.append(znak + "%%%" + novoStanje + "&&&");
				promjena = true;
			}
			if (promjena)
				sb.delete(sb.length() - 3, sb.length() + 1);
			izlaz.println(sb);
		}
		izlaz.println("=====");
		for (int i = 0; i < sinkronizacijskiZnakovi.length; i++) {
			if (i < sinkronizacijskiZnakovi.length - 1) {
				izlaz.print(sinkronizacijskiZnakovi[i] + " ");
			} else {
				izlaz.print(sinkronizacijskiZnakovi[i]);
			}
		}
		izlaz.close();
	}

	public void pomakniStavi(Gramatika gramatika, DKA dka) {
		for (DKA.Prijelaz prijelaz : dka.prijelazi) {
			// nezavrsni znakovi
			if (gramatika.nezav.contains(prijelaz.znak)) {
				dodajNovoStanje(prijelaz.pocetno.index, prijelaz.znak,
						prijelaz.konacno.index);
			}
			// zavrsni
			else {
				if (prijelaz.konacno.index == 0) {
					// sup
					System.out.println();
				}
				dodajAkcijuPomakni(prijelaz.pocetno.index, prijelaz.znak,
						prijelaz.konacno.index);
			}
		}
	}

	private void ReducirajPrihvati(Gramatika gramatika, DKA dka) {
		for (DKAStanje stanje : dka.stanja) {
			HashSet<String> neiskoristeni = new HashSet<String>();
			neiskoristeni.addAll(gramatika.zav);
			Map<String, Akcija> znakAkcija = akcija.get(stanje.index);
			if (znakAkcija != null) {
				neiskoristeni.removeAll(znakAkcija.keySet());
			}
			for (String znak : neiskoristeni) {
				LRStavka najveciPrioritet = null;
				for (LRStavka stavka : stanje.elementi) {
					// TODO zbog epslion produkcija
					// nije todo nego obavijest
					int brojZnakovaDesneStrane = 0;
					if (!stavka.desnaStrana.equals("")) {
						brojZnakovaDesneStrane = stavka.brojZnakovaDesneStrane;
					}
					if (brojZnakovaDesneStrane == stavka.tockaIndex
							&& stavka.skupDesneStrane.contains(znak)) {
						// TODO znak kraja niza
						if (znak.equals("\27")
								&& stavka.lijevaStrana.equals("<%>")) {
							dodajAkcijuPrihvati(stanje.index, znak);
							break;
						}
						if (stavka.lijevaStrana.equals("<%>")) {
							continue;
						}
						if (veciPrioritet(stavka, najveciPrioritet)) {
							najveciPrioritet = stavka;
						}
					}
				}
				if (najveciPrioritet != null) {
					dodajAkcijuReduciraj(stanje.index, znak, najveciPrioritet);
				}
			}
			stanja.add(stanje.index);
		}
	}

	private boolean veciPrioritet(LRStavka stavka, LRStavka najveciPrioritet) {
		if (najveciPrioritet == null) {
			return true;
		}
		if (stavka.produkcija.redniBroj < najveciPrioritet.produkcija.redniBroj)
			System.err.println(stavka + " VS " + najveciPrioritet);
		return false;
	}

	private void dodajAkcijuPrihvati(Integer stanje, String znak) {
		if (!akcija.containsKey(stanje)) {
			akcija.put(stanje, new LinkedHashMap<String, Akcija>());
		}
		Map<String, Akcija> map = akcija.get(stanje);
		map.put(znak, new Prihvati());
		akcija.put(stanje, map);
	}

	private void dodajAkcijuReduciraj(Integer stanje, String znak,
			LRStavka stavka) {
		if (!akcija.containsKey(stanje)) {
			akcija.put(stanje, new LinkedHashMap<String, Akcija>());
		}
		Map<String, Akcija> map = akcija.get(stanje);
		map.put(znak, new Reduciraj(stavka));
		akcija.put(stanje, map);
	}

	private void dodajAkcijuPomakni(Integer pocetno, String znak,
			Integer konacno) {
		if (!akcija.containsKey(pocetno)) {
			akcija.put(pocetno, new LinkedHashMap<String, Akcija>());
		}
		Map<String, Akcija> map = akcija.get(pocetno);
		map.put(znak, new Pomakni(konacno));
		akcija.put(pocetno, map);
	}

	private void dodajNovoStanje(Integer pocetno, String znak, Integer konacno) {
		if (!novoStanje.containsKey(pocetno)) {
			novoStanje.put(pocetno, new LinkedHashMap<String, Integer>());
		}
		Map<String, Integer> map = novoStanje.get(pocetno);
		map.put(znak, konacno);
		novoStanje.put(pocetno, map);
	}

	public class Prihvati extends Akcija {
		@Override
		public String toString() {
			return new String("a");
			// return new String("Prihvati()");
		}
	}

	public class Reduciraj extends Akcija {
		public String produkcija;

		public Reduciraj(LRStavka stavka) {
			if (stavka.desnaStrana.equals("")) {
				this.produkcija = new String(stavka.lijevaStrana + "->"
						+ stavka.desnaStrana);
			} else {
				StringBuilder sb = new StringBuilder();
				sb.append(stavka.lijevaStrana + "->");
				for (int i = 0, l = stavka.produkcija.znakoviDesneStrane.length; i < l; i++) {
					if (i < l - 1) {
						sb.append(stavka.produkcija.znakoviDesneStrane[i] + " ");
					} else {
						sb.append(stavka.produkcija.znakoviDesneStrane[i]);
					}
				}
				produkcija = sb.toString();
			}
		}

		public Reduciraj(String produkcija) {
			this.produkcija = produkcija;
		}

		@Override
		public String toString() {
			return new String("r(" + produkcija + ")");
			// return new String("Reduciraj("+produkcija+")");
		}
	}

	public class Pomakni extends Akcija {
		public int konacno;

		public Pomakni(int konacno) {
			this.konacno = konacno;
		}

		@Override
		public String toString() {
			// return new String("Pomakni("+konacno+")");
			return new String("p" + konacno);
		}
	}

	public class Akcija {
		/*
		 * @Override public String toString() { return new String("Odbaci()"); }
		 */
	}

}
