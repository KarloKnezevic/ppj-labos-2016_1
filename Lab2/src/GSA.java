import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

// TODO napraviti ENKA

public class GSA {

	public static void main(String args[]) throws Exception {
		// ucitavanje datoteke
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));
		String line;

		// lines sadrzi sve ucitane linije
		List<String> lines = new ArrayList<String>();
		while ((line = reader.readLine()) != null) {
			if (line.equals(""))
				break;
			lines.add(line);
		}

		// nezavrsni znakovi
		String[] nezavrsniZnakovi = lines.get(0).substring(3).trim().split(" ");
		List<String> nezavrsni = new ArrayList<String>(
				Arrays.asList(nezavrsniZnakovi));

		// dodavanje pocetnog nezavrsnog znaka
		nezavrsni.add(0, "<%>");

		// zavrsni znakovi
		String[] zavrsniZnakovi = lines.get(1).substring(3).trim().split(" ");
		List<String> zavrsni = Arrays.asList(zavrsniZnakovi);

		// sinkronizacijski znakovi
		String[] sinkronizacijskiZnakovi = lines.get(2).substring(5).trim()
				.split(" ");

		// prva tri reda vise ne trebaju; u lines ostanu samo produkcije
		lines.remove(0);
		lines.remove(0);
		lines.remove(0);

		// ovdje ce biti sve produkcije oznacene globalnim indexom
		List<Produkcija> produkcije = new ArrayList<Produkcija>();
		int redniBroj = 1;

		// stvaranje produkcija
		for (int i = 0; i < lines.size(); i++) {
			String lin = lines.get(i);
			// ako je lijeva strana produkcije
			if (lin.startsWith("<")) {
				i++;
				String desnaStrana = lines.get(i);
				while (!desnaStrana.startsWith("<")) {
					produkcije.add(new Produkcija(lin.trim(), desnaStrana
							.trim(), redniBroj));
					redniBroj++;
					i++;
					// dosli smo stvarno do kraja
					try {
						desnaStrana = lines.get(i);
					} catch (Exception e) {
						break;
					}
				}
				// mora biti -- jer vec nadje sljedecu ljevu stranu produkcije a
				// for petlja ga uveca jos jednom
				i--;
			}
		}

		// dodavanje pocetne produkcije
		produkcije
				.add(0, new Produkcija(nezavrsni.get(0), nezavrsni.get(1), 0));

		Gramatika gramatika = new Gramatika(zavrsni, nezavrsni, produkcije);

		// izracun relacije zapocinje znakom
		TablicaRelacije tablica = zapocinjeZnakom(gramatika);

		long time = System.currentTimeMillis();

		ENKA enka = new ENKA(gramatika, tablica);
		long time2 = System.currentTimeMillis();
		System.err.println((time2 - time) / 1000.0 + "s, ENKA stanja: "
				+ enka.stanja.size() + ", ENKA prijelaza: "
				+ enka.prijelazi.size());

		DKA papedka = new DKA(enka);
		long time3 = System.currentTimeMillis();
		System.err.println((time3 - time2) / 1000.0 + "s, DKA stanja: "
				+ papedka.stanja.size() + ", DKA prijelaza: "
				+ papedka.prijelazi.size());

		// System.setOut(new PrintStream(new FileOutputStream(new
		// File("D:\\DKA.txt"))));
		// System.out.println(papedka.toString());

		TablicaAkcijaNovoStanje tablicaAkcijaNovoStanje = new TablicaAkcijaNovoStanje(
				gramatika, papedka, sinkronizacijskiZnakovi);

		/*
		 * NKA nka = new NKA(enka); System.out.println(nka.toString());
		 * 
		 * DKA dka = new DKA(nka); System.out.println(dka.toString());
		 */

	}

	// vraca TablicuRelacije zapocinje znakom kao u knjizi
	public static TablicaRelacije zapocinjeZnakom(Gramatika gramatika) {
		// znakovi sadrzi sve znakove
		List<String> znakovi = new ArrayList<String>(gramatika.svi);
		List<String> nezavrsni = gramatika.nezavrsni;

		// vrijednost koja se vraca
		TablicaRelacije tablica = new TablicaRelacije(gramatika.nezavrsni,
				gramatika.zavrsni);

		// za sve znakove nezavrsne izracunaj relaciju zapocinje izravno u
		// odnosu na sve znakove
		for (String A : nezavrsni) {
			for (String B : znakovi) {
				if (!A.equals(B)) {
					zapocinjeIzravnoZnakom(gramatika, A, B, tablica);
				}
			}
		}

		int vel = tablica.index.size();

		// refleksivna relacija. oznacujemo dijagonalu.
		for (int i = 0; i < vel; i++) {
			tablica.tablica[i][i] = true;
		}

		// ponavljamo n puta
		for (int n = 0; n < vel; n++) {
			// ako je oznacen redak j i stupac k, te ako je oznacen redak k i
			// stupac
			// l, onda se oznaci redak j i stupac l
			for (int j = 0; j < vel; j++) {
				for (int k = 0; k < vel; k++) {
					for (int l = 0; l < vel; l++) {
						if (tablica.tablica[j][k] && tablica.tablica[k][l]) {
							tablica.tablica[j][l] = true;
						}
					}
				}
			}

		}

		return tablica;
	}

	// kao u knjizi, gleda da li znak b zapocinje niz beta.
	// predaje mu se tablica koju generira zapocinjeZnakom
	public static boolean zapocinjeNiz(String b, String beta,
			TablicaRelacije tablica) {
		if (beta.startsWith(b)) {
			return true;
		} else if (beta.startsWith("<")) {
			// pronadji prvi nezavrsni znak
			String nezavrsniZnak = beta.substring(0, beta.indexOf('>') + 1);
			if (tablica.jeOznacen(b, nezavrsniZnak)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	// trazi prazne nezavrsne znakove u danim produkcijama
	// NE KORISTI SE (osim za generiranje zapocinje izravno znakom)
	public static List<String> prazniNezavrsniZnakovi(
			List<Produkcija> produkcije) {
		Set<String> prazni = new LinkedHashSet<String>();

		// dodaju se sve epsilon produkcije
		for (Produkcija prod : produkcije) {
			if (prod.desnaStrana.equals("")) {
				prazni.add(prod.lijevaStrana);
			}
		}

		// ako su svi znakovi desne strane prazni, dodaj ljevu stranu u listu
		// praznih i tako dok god se mjenja lista
		boolean bilaPromjena = true;
		while (bilaPromjena) {
			bilaPromjena = false;
			for (Produkcija prod : produkcije) {
				if (prazni.containsAll(Arrays.asList(prod.znakoviDesneStrane))) {
					bilaPromjena = prazni.add(prod.lijevaStrana);
				}
			}
		}

		return new ArrayList<String>(prazni);
	}

	// racuna relaciju zapocinje izravno znakom i ispunjava danu tablicu
	// NE KORISTI SE (osim za generiranje zapocinje znakom)
	public static void zapocinjeIzravnoZnakom(Gramatika gramatika, String A,
			String B, TablicaRelacije tablica) {
		List<Produkcija> produkcije = gramatika.produkcije;

		// trebaju nam prazni znakovi
		List<String> prazni = prazniNezavrsniZnakovi(produkcije);

		// optimizacija da ne trazimo po svim produkcijama nego samo ono sto
		// zapravo trebamo
		List<Produkcija> relevantne = new ArrayList<Produkcija>();
		for (Produkcija prod : produkcije) {
			if (prod.lijevaStrana.equals(A)) {
				relevantne.add(prod);
			}
		}

		// trazi po relevantnim produkcijama
		for (Produkcija prod : relevantne) {
			// ako ljeva strana ima trazeni B
			if (prod.desnaStrana.contains(B)) {
				for (String znak : prod.znakoviDesneStrane) {
					// onda taj B mora biti ili prvi
					if (znak.equals(B)) {
						tablica.oznaci(A, B);
						break;
					} else {
						// ili svi znakovi ispred njega moraju biti prazni
						if (prazni.contains(znak)) {
							// ako jesu ne ucini nista
						} else {
							// ako nisu probaj trazit dalje
							break;
						}
					}
				}
			}
		}
	}
}
