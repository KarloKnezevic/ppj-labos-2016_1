import java.util.List;


/**
 * Metode za analizu naredbi (poglavlje 4.4.5. u uputama)
 * @author wex
 *
 */
public abstract class Command {
	//TODO tu treba dodati jos promjene scopeova u nekim metodama
	
	public static boolean checkIfThisFunctionType(TreeNode node, String type){
		if(!node.getName().equals("<definicija_funkcije>")){
			if(node.getParent() == null)
				return false;
			else
				return checkIfThisFunctionType(node.getParent(), type);
		}
		else{
			if(!node.getChild(0).getName().equals("<ime_tipa>"))
				return false;
			else
				node = node.getChild(0);
			
			if(!node.getChild(0).getName().equals("<specifikator_tipa>"))
				return false;
			else
				node = node.getChild(0).getChild(0);
			
			if(node.getName().equals("KR_VOID") && type.equals("void"))
				return true;
			else if(Expression.isImplicitlyCastable(type, node.getLex_atom()))
				return true;
		}
		return false;
	}
	
	
	/**
	 * @author wex
	 * @param currentNode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean slozena_naredba(TreeNode node){
		List<String> types = null;
		List<String> names = null;
		if(!Analyzator.funcParamsStack.isEmpty()){
			types = (List<String>) Analyzator.funcParamsStack.pop();
			names = (List<String>) Analyzator.funcParamsStack.pop();
		}
		
		Analyzator.currentScope = new Scope(Analyzator.currentScope); //specificno za ovaj nezavrsni znak, citaj uputu...
		
		if(types != null)
			for(int i = 0; i < types.size(); i++)
				Analyzator.currentScope.addVariable(names.get(i), true, types.get(i)); //TODO MOZDA BUG????
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("L_VIT_ZAGRADA") 
			&& node.getChild(1).getName().equals("<lista_naredbi>")){
			
			// 1. provjeri(<lista_naredbi>)
			lista_naredbi(node.getChild(1));
			
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("L_VIT_ZAGRADA")
				&& node.getChild(1).getName().equals("<lista_deklaracija>")){
			
			// 1. provjeri(<lista_deklaracija>)
			Declaration.lista_deklaracija(node.getChild(1));
			
			// 2. provjeri(<lista_naredbi>)
			lista_naredbi(node.getChild(2));
		}
		
		Analyzator.currentScope = Analyzator.currentScope.getParent();
		return true;
	}

	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean lista_naredbi(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("<naredba>")){
			
			// 1. provjeri(<naredba>)
			naredba(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("<lista_naredbi>")){
			
			// 1. provjeri(<lista_naredbi>)
			lista_naredbi(node.getChild(0));
			
			// 2. provjeri(<naredba>)
			naredba(node.getChild(1));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean naredba(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("<slozena_naredba>")){
			
			// 1. provjeri(<slozena_naredba>)
			slozena_naredba(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("<izraz_naredba>")){
			
			// 1. provjeri(<izraz_naredba>)
			izraz_naredba(node.getChild(0));
		}
		
		// 3. produkcija
		else if(node.getChild(0).getName().equals("<naredba_grananja>")){

			// 1. provjeri(<naredba_grananja>)
			naredba_grananja(node.getChild(0));
		}
		
		// 4. produkcija
		else if(node.getChild(0).getName().equals("<naredba_petlje>")){

			// 1. provjeri(<naredba_petlje>)
			naredba_petlje(node.getChild(0));
		}
		
		// 5. produkcija
		else if(node.getChild(0).getName().equals("<naredba_skoka>")){

			// 1. provjeri(<naredba_skoka>)
			naredba_skoka(node.getChild(0));
		}
		
		return true;
	}
	
	/**
	 * Ova analiza ce staviti na Analyzator.stack svoje svojstvo "tip" (String)
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean izraz_naredba(TreeNode node){
		String tip;
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("TOCKAZAREZ")){
			
			// nema provjera
			tip = "int";
			Analyzator.stack.push(tip); //jel ovo nuzno?
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("<izraz>")){
			
			// 1. provjeri(<izraz>)
			Expression.izraz(node.getChild(0));
			
			//tu bi samo pop-ao tip sa stacka i ponovno to isto pushao
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean naredba_grananja(TreeNode node){
		String tip;
		
		// 1. produkcija
		if(node.getChildrenAmount() == 5){
			
			// 1. provjeri(<izraz>)
			Expression.izraz(node.getChild(2));
			
			if(Analyzator.stack.isEmpty())
				Analyzator.printErr(node);
			Object undefined = Analyzator.stack.pop();
			if(!(undefined instanceof String))
				Analyzator.printErr(node);
			
			tip = (String) undefined;
			
			// 2. <izraz>.tip ~ int
			if(!(tip.equals("int") || tip.equals("char") || tip.equals("const(int)") || tip.equals("const(char")))
				Analyzator.printErr(node);
			
			// 3. provjeri(<naredba>)
			naredba(node.getChild(4));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 7){
			
			// 1. provjeri(<izraz>)
			Expression.izraz(node.getChild(2));
			
			tip = (String) Analyzator.stack.pop();
			
			// 2. <izraz>.tip ~ int
			if(!(tip.equals("int") || tip.equals("char") || tip.equals("const(int)") || tip.equals("const(char")))
				Analyzator.printErr(node);
			
			// 3. provjeri(<naredba>1)
			naredba(node.getChild(4));
			
			// 4. provjeri(<naredba>2)
			naredba(node.getChild(6));
		}
		
		return true;
	}

	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean naredba_petlje(TreeNode node){
		String tip = "";
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("KR_WHILE")){
			
			// 1. provjeri(<izraz>)
			Expression.izraz(node.getChild(2));
			
			tip = (String) Analyzator.stack.pop();
			
			// 2. <izraz>.tip ~ int
			if(!(tip.equals("int") || tip.equals("char") || tip.equals("const(int)") || tip.equals("const(char")))
				Analyzator.printErr(node);
			
			// 3. provjeri(<naredba>)
			naredba(node.getChild(4));
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("KR_FOR") && node.getChildrenAmount() == 6){
			
			// 1. provjeri(<izraz_naredba>1)
			izraz_naredba(node.getChild(2));
			
			// 2. provjeri(<izraz_naredba>2)
			izraz_naredba(node.getChild(3));
			tip = (String) Analyzator.stack.pop();
			
			// 3. <izraz_naredba>2.tip ~ int
			if(!(tip.equals("int") || tip.equals("char") || tip.equals("const(int)") || tip.equals("const(char)")))
				Analyzator.printErr(node);
			
			// 4. provjeri(<naredba>)
			naredba(node.getChild(5));
		}
		
		// 3. produkcija
		else if(node.getChild(0).getName().equals("KR_FOR") && node.getChildrenAmount() == 7){

			// 1. provjeri(<izraz_naredba>1)
			izraz_naredba(node.getChild(2));
			
			// 2. provjeri(<izraz_naredba>2)
			izraz_naredba(node.getChild(3));
			tip = (String) Analyzator.stack.pop();
			
			// 3. <izraz_naredba>2.tip ~ int
			if(!(tip.equals("int") || tip.equals("char") || tip.equals("const(int)") || tip.equals("const(char")))
				Analyzator.printErr(node);
			
			// 4. provjeri(<izraz>)
			Expression.izraz(node.getChild(4));
			
			// 5. provjeri(<naredba>)
			naredba(node.getChild(6));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean naredba_skoka(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("KR_CONTINUE")){
			
			// 1. provjeri je li unutar petlje
			TreeNode cur = node;
			while((cur = cur.getParent()) != null)
				if(cur.getName().equals("<naredba_petlje>"))
					return true;
			
			Analyzator.printErr(node);
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("KR_BREAK")){
			
			// 1. provjeri je li unutar petlje
			TreeNode cur = node;
			while((cur = cur.getParent()) != null)
				if(cur.getName().equals("<naredba_petlje>"))
					return true;
			
			Analyzator.printErr(node);
		}
		
		// 3. produkcija
		else if(node.getChild(0).getName().equals("KR_RETURN") && node.getChildrenAmount() == 2){
			
			// 1. provjeri je li naredba unutar funkcije tipa funkcija(params -> void)
			if(!checkIfThisFunctionType(node, "void"))
				Analyzator.printErr(node);
		}
		
		// 4. produkcija
		else if(node.getChild(0).getName().equals("KR_RETURN") && node.getChildrenAmount() == 3){
			String type;
			
			Expression.izraz(node.getChild(1));
			type = (String) Analyzator.stack.pop();
			if(type.equals("kriva_funkcija"))
				Analyzator.printErr(node);
			
			// 1. provjeri je li naredba unutar funkcije tipa funkcija(params -> pov) i je li <izraz>.tip ~ pov
			if(!checkIfThisFunctionType(node, type))
				Analyzator.printErr(node);
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean prijevodna_jedinica(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<vanjska_deklaracija>)
			vanjska_deklaracija(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 2){
			
			// 1. provjeri(<prijevodna_jedinica>)
			prijevodna_jedinica(node.getChild(0));
			
			// 2. provjeri(<vanjska_deklaracija>)
			vanjska_deklaracija(node.getChild(1));
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean vanjska_deklaracija(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("<definicija_funkcije>")){
			
			// 1. provjeri(<definicija_funkcije>)
			Declaration.definicija_funkcije(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("<deklaracija>")){
			
			// 1. provjeri(<deklaracija>)
			Declaration.deklaracija(node.getChild(0));
		}
		
		return true;
	}

}
