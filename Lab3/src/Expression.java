import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * Metode za analizu izraza (poglavlje 4.4.4. u uputama)
 * @author wex
 *
 */
public abstract class Expression {
	
	/**
	 * Pomocna metoda, provjerava relaciju (type ~ int) citaj uputu...
	 * @author wex
	 * @param type
	 * @return
	 */
	public static boolean isImplicitlyCastableToInt(String type){
		return type.equals("int") || type.equals("char") || type.equals("const(int)") || type.equals("const(char)");
	}
	
	/**
	 * Pomocna metoda, provjerava jel moguce castati (implicitno ili eksplicitno)
	 * iz tipa fromType u tip toType, poglavlje 4.3.1 u uputi...
	 * 
	 * @param fromType
	 * @param toType
	 * @return
	 */
	public static boolean isCastable(String fromType, String toType){
		if(fromType.equals("int") && toType.equals("char"))
			return true;
		if(fromType.equals("int") && toType.equals("const(char)"))
			return true;
		if(fromType.equals("const(int)") && toType.equals("const(char)"))
			return true;
		if(fromType.equals("const(int)") && toType.equals("char"))
			return true;
		if(isImplicitlyCastable(fromType, toType))
			return true;
		return false;
	}
	
	/**
	 * Pomocna metoda, provjerava jel moguce implicitno castati iz tipa
	 * fromType u tip toType...
	 * 
	 * @param fromType
	 * @param toType
	 * @return
	 */
	public static boolean isImplicitlyCastable(String fromType, String toType){
		if(fromType.equals(toType))
			return true;
		if(fromType.equals("char"))
			if(toType.equals("int") || toType.equals("const(char)") || toType.equals("const(int)"))
				return true;
		if(fromType.equals("const(char)"))
			if(toType.equals("int") || toType.equals("char") || toType.equals("const(int)"))
				return true;
		if(fromType.equals("int"))
			if(toType.equals("const(int)"))
				return true;
		if(fromType.equals("const(int)"))
			if(toType.equals("int"))
				return true;
		if(fromType.equals("niz(int)"))
			if(toType.equals("niz(const(int))"))
				return true;
		if(fromType.equals("niz(char)"))
			if(toType.equals("niz(const(char))"))
				return true;
		return false;
	}
	
	/**
	 * Nakon poziva ove metode ona stavlja prvo l_izraz na stog pa onda tip
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean primarni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("IDN")){
			
			// 1. IDN.ime deklarirano
			Var var = Analyzator.currentScope.findVariable(node.getChild(0).getLex_atom());
			if(var == null)
				Analyzator.printErr(node);
			
			if(var.isFunction()){
				Analyzator.functionCallStack.push(var);
				Analyzator.stack.push("kriva_funkcija");
			}
			else{
				Analyzator.stack.push(var.isL_type());
				Analyzator.stack.push(var.getType());
			}
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("BROJ")){
			
			// 1. vrijednost je u rasponu INT-a
			String stringValue = node.getChild(0).getLex_atom();
			BigInteger value;
			// ako imamo heksadekadski broj treba ga pretvorit u dekadski
			if(stringValue.startsWith("0") && stringValue.length() > 1){
				if(stringValue.contains("x")){
					value = new BigInteger(stringValue.substring(2), 16);
				}else{
					value = new BigInteger(stringValue.substring(1), 8);
				}
			}else{
				value = new BigInteger(stringValue);
			}
			
			
			if(!(value.compareTo(new BigInteger("-2147483649")) == 1 && value.compareTo(new BigInteger("2147483648")) == -1))
				Analyzator.printErr(node);
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		// 3. produkcija
		else if(node.getChild(0).getName().equals("ZNAK")){
			
			// 1. znak je ispravan po 4.3.2.
			String znak = node.getChild(0).getLex_atom();
			
			if(!(znak.startsWith("'") && znak.endsWith("'")))
				Analyzator.printErr(node);
			znak = znak.substring(1, znak.length()-1);
			
			if(znak.length() == 2)
				if(!(znak.charAt(0) == '\\' && (znak.charAt(1) == 't' || znak.charAt(1) == 'n' 
						|| znak.charAt(1) == '0' || znak.charAt(1) == '\'' || znak.charAt(1) == '"'
						|| znak.charAt(1) == '\\')))
						Analyzator.printErr(node);
			if(znak.equals("\\"))
					Analyzator.printErr(node);
			Analyzator.stack.push(false);
			Analyzator.stack.push("char");
		}
		
		// 4. produkcija
		else if(node.getChild(0).getName().equals("NIZ_ZNAKOVA")){
			
			// 1. konstantni niz znakova je ispravan po 4.3.2.
			String niz = node.getChild(0).getLex_atom();
			
			if(!(niz.startsWith("\"") && niz.endsWith("\"")))
				Analyzator.printErr(node);
			
			boolean prefix = false;
			for(char c : niz.substring(1, niz.length()-1).toCharArray()){
				if(prefix)
					if(!(c == 't' || c == 'n' || c == '0' || c == '\'' || c == '"' || c == '\\'))
						Analyzator.printErr(node);
					else
						prefix = false;
				else if(c == '\\')
					prefix = true;
				else if(c == '"')
					Analyzator.printErr(node);
			}
			if(prefix)
				Analyzator.printErr(node);
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("niz(const(char))");
		}
		
		// 5. produkcija
		else if(node.getChild(0).getName().equals("L_ZAGRADA")){
			
			// 1. provjeri(<izraz>)
			izraz(node.getChild(1));
			//nema potrebe za muljanje sa stackom...
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean postfiks_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("<primarni_izraz>")){
			
			// 1. provjeri(<primarni_izraz>)
			primarni_izraz(node.getChild(0));
			//nema potrebe za muljanje sa stackom...
		}
		
		// 2. produkcija
		else if(node.getChild(1).getName().equals("L_UGL_ZAGRADA")){
			
			// 1. provjeri(<postfiks_izraz>)
			postfiks_izraz(node.getChild(0));
			
			// 2. <postfiks_izraz>.tip = niz(X)
			String type = (String) Analyzator.stack.pop();
			if(!type.startsWith("niz"))
				Analyzator.printErr(node);
			
			type = type.substring(4, type.length()-1);
			boolean l_type = type.startsWith("const") ? false : true;
			
			// 3. provjeri(<izraz>)
			izraz(node.getChild(2));
			String exprType = (String) Analyzator.stack.pop();
			
			// 4. <izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt(exprType)))
				Analyzator.printErr(node);
			
			Analyzator.stack.push(l_type);
			Analyzator.stack.push(type);
		}
		
		// 3. produkcija
		else if(node.getChild(1).getName().equals("L_ZAGRADA") && node.getChild(2).getName().equals("D_ZAGRADA")){
			
			// 1. provjeri(<postfiks_izraz>)
			postfiks_izraz(node.getChild(0));
			
			// 2. <postfiks_izraz>.tip = funkcija(void->pov)
	
			Var func = (Var) Analyzator.functionCallStack.pop();
			if(!func.getParams().isEmpty())
				Analyzator.printErr(node);
			Analyzator.stack.push(false);
			Analyzator.stack.push(func.getType());
		}
		
		// 4. produkcija
		else if(node.getChild(1).getName().equals("L_ZAGRADA") && node.getChild(2).getName().equals("<lista_argumenata>")){
			
			// 1. provjeri(<postfiks_izraz>)
			postfiks_izraz(node.getChild(0));
			if(Analyzator.functionCallStack.isEmpty())
				Analyzator.printErr(node);
			Var func = (Var) Analyzator.functionCallStack.pop();
			
			// 2. provjeri(<lista_argumenata>)
			lista_argumenata(node.getChild(2));
			
			@SuppressWarnings("unchecked")
			List<String> types = (List<String>) Analyzator.stack.pop();
			// 3. pogledaj uputu za ovu provjeru...
			List<String> funcTypes;
			if((funcTypes = func.getParams()) == null)
				Analyzator.printErr(node);
			if(!(funcTypes.size() == types.size()))
				Analyzator.printErr(node);
			for(int i = 0; i < types.size(); i++)
				if(!isImplicitlyCastable(types.get(i), funcTypes.get(i)))
						Analyzator.printErr(node);
			
			Analyzator.stack.push(false);
			Analyzator.stack.push(func.getType());
		}
		
		// 5. i 6. produkcija
		else if(node.getChild(1).getName().equals("OP_INC") || node.getChild(1).getName().equals("OP_DEC")){
			
			// 1. provjeri(<postfiks_izraz>)
			postfiks_izraz(node.getChild(0));
			
			// 2. <postfiks_izraz>.l-izraz = true && <postfiks_izraz>.tip ~ int
			String type = (String) Analyzator.stack.pop();
			boolean l_type = (boolean) Analyzator.stack.pop();
			
			if(!(l_type && isImplicitlyCastableToInt(type)))
				Analyzator.printErr(node);
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean lista_argumenata(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<izraz_pridruzivanja>)
			izraz_pridruzivanja(node.getChild(0));
			List<String> types = new ArrayList<String>();
			types.add((String) Analyzator.stack.pop());
			Analyzator.stack.push(types);
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<lista_argumenata>)
			lista_argumenata(node.getChild(0));
			@SuppressWarnings("unchecked")
			List<String> types = (List<String>) Analyzator.stack.pop();
			
			// 2. provjeri(<izraz_pridruzivanja>)
			izraz_pridruzivanja(node.getChild(2));
			types.add((String) Analyzator.stack.pop());
			Analyzator.stack.push(types);
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean unarni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<postfiks_izraz>)
			postfiks_izraz(node.getChild(0));
			//ne treba nista po stogu muljati...
		}
		
		// 2. i 3. produkcija
		else if(node.getChild(1).getName().equals("<unarni_izraz>")){
			
			// 1. provjeri(<unarni_izraz>)
			unarni_izraz(node.getChild(1));
			
			// 2. <unarni_izraz>.l_izraz = true && <unarni_izraz>.tip ~ int
			String type = (String) Analyzator.stack.pop();
			boolean l_type = (boolean) Analyzator.stack.pop();
			
			if(!(l_type && isImplicitlyCastableToInt(type)))
				Analyzator.printErr(node);
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		// 4. produkcija
		else if(node.getChild(1).getName().equals("<cast_izraz>")){
			
			// 1. provjeri(<cast_izraz>)
			cast_izraz(node.getChild(1));
			
			// 2. <cast_izraz>.tip ~ int
			String type = (String) Analyzator.stack.pop();
			if(!isImplicitlyCastableToInt(type))
				Analyzator.printErr(node);
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean cast_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<unarni_izraz>)
			unarni_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 4){
			
			// 1. provjeri(<ime_tipa>)
			ime_tipa(node.getChild(1));
			String type_name = (String) Analyzator.stack.pop();
			
			// 2. provjeri(<cast_izraz>)
			cast_izraz(node.getChild(3));
			
			if(Analyzator.stack.isEmpty())
				Analyzator.printErr(node);
			Object undefined = Analyzator.stack.pop();
			if(!(undefined instanceof String))
				Analyzator.printErr(node);
			
			String type_cast = (String) undefined;
			Analyzator.stack.pop();
			
			// 3. <cast_izraz>.tip moze se pretvoriti u <ime_tipa>.tip po poglavlju 4.3.1
			if(!isCastable(type_cast, type_name))
				Analyzator.printErr(node);
			
			Analyzator.stack.push(false);
			Analyzator.stack.push(type_name);
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean ime_tipa(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<specifikator_tipa>)
			specifikator_tipa(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 2){
			
			// 1. provjeri(<specifikator_tipa>)
			specifikator_tipa(node.getChild(1));
			
			String type = (String) Analyzator.stack.pop();
			
			if(type.equals("void"))
				Analyzator.printErr(node);
			
			Analyzator.stack.push("const(" + type + ")");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean specifikator_tipa(TreeNode node){
		
		// 1. produkcija
		if(node.getChild(0).getName().equals("KR_VOID")){
			
			Analyzator.stack.push("void");
		}
		
		// 2. produkcija
		else if(node.getChild(0).getName().equals("KR_CHAR")){
			
			Analyzator.stack.push("char");
		}
		
		// 3. produkcija
		else if(node.getChild(0).getName().equals("KR_INT")){
			
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean multiplikativni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<cast_izraz>)
			cast_izraz(node.getChild(0));
		}
		
		// 2. 3. i 4. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<multiplikativni_izraz>)
			multiplikativni_izraz(node.getChild(0));
			
			// 2. <multiplikativni_izraz>.tip ~ int
			if(!isImplicitlyCastableToInt((String) Analyzator.stack.pop()))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			// 3. provjeri(<cast_izraz>)
			cast_izraz(node.getChild(2));
			
			// 4. <cast_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean aditivni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<multiplikativni_izraz>)
			multiplikativni_izraz(node.getChild(0));
		}
		
		// 2. i 3. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<aditivni_izraz>)
			aditivni_izraz(node.getChild(0));
			
			// 2. <aditivni_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			// 3. provjeri(<multiplikativni_izraz>)
			multiplikativni_izraz(node.getChild(2));
			
			// 4. <multiplikativni_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean odnosni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<aditivni_izraz>)
			aditivni_izraz(node.getChild(0));
		}
		
		// 2. 3. 4. i 5. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<odnosni_izraz>)
			odnosni_izraz(node.getChild(0));
			
			// 2. <odnosni_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			// 3. provjeri(<aditivni_izraz>)
			aditivni_izraz(node.getChild(2));
			
			// 4. <aditivni_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean jednakosni_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<odnosni_izraz>)
			odnosni_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<jednakosni_izraz>)
			jednakosni_izraz(node.getChild(0));
			
			if(Analyzator.stack.isEmpty())
				Analyzator.printErr(node);
			Object undefined = Analyzator.stack.pop();
			if(!(undefined instanceof String))
				Analyzator.printErr(node);
			
			// 2. <jednakosni_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) undefined)))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			// 3. provjeri(<odnosni_izraz>)
			odnosni_izraz(node.getChild(2));
			
			// 4. <odnosni_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean bin_i_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<jednakosni_izraz>)
			jednakosni_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<bin_i_izraz>)
			bin_i_izraz(node.getChild(0));
			
			// 2. <bin_i_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			// 3. provjeri(<jednakosni_izraz>)
			jednakosni_izraz(node.getChild(2));
			
			// 4. <jednakosni_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean bin_xili_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<bin_i_izraz>)
			bin_i_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){

			// 1. provjeri(<bin_xili_izraz>)
			bin_xili_izraz(node.getChild(0));

			// 2. <bin_xili_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			// 3. provjeri(<bin_i_izraz>)
			bin_i_izraz(node.getChild(2));

			// 4. <bin_i_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean bin_ili_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<bin_xili_izraz>)
			bin_xili_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){

			// 1. provjeri(<bin_ili_izraz>)
			bin_ili_izraz(node.getChild(0));

			// 2. <bin_ili_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			// 3. provjeri(<bin_xili_izraz>)
			bin_xili_izraz(node.getChild(2));

			// 4. <bin_xili_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean log_i_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<bin_ili_izraz>)
			bin_ili_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){

			// 1. provjeri(<log_i_izraz>)
			log_i_izraz(node.getChild(0));

			// 2. <log_i_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			// 3. provjeri(<bin_ili_izraz>)
			bin_ili_izraz(node.getChild(2));

			// 4. <bin_ili_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean log_ili_izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<log_i_izraz>)
			log_i_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){

			// 1. provjeri(<log_ili_izraz>)
			log_ili_izraz(node.getChild(0));

			// 2. <log_ili_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			// 3. provjeri(<log_i_izraz>)
			log_i_izraz(node.getChild(2));

			// 4. <log_i_izraz>.tip ~ int
			if(!(isImplicitlyCastableToInt((String) Analyzator.stack.pop())))
				Analyzator.printErr(node);
			Analyzator.stack.pop();
			
			Analyzator.stack.push(false);
			Analyzator.stack.push("int");
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean izraz_pridruzivanja(TreeNode node){
		
		// 1. produkcija 
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<log_ili_izraz>)
			log_ili_izraz(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<postfiks_izraz>)
			postfiks_izraz(node.getChild(0));
			String postfix_type = (String) Analyzator.stack.pop();
			
			// 2. <postfiks_izraz>.l-izraz = true
			if(!((boolean) Analyzator.stack.pop()))
				Analyzator.printErr(node);
			
			// 3. provjeri(<izraz_pridruzivanja>)
			izraz_pridruzivanja(node.getChild(2));
			String type = (String) Analyzator.stack.pop();
			
			// 4. <izraz_pridruzivanja>.tip ~ <postfiks_izraz>.tip
			if(!(isImplicitlyCastable(type, postfix_type)))
				Analyzator.printErr(node);
			
			Analyzator.stack.push(false);
			Analyzator.stack.push(postfix_type);
		}
		
		return true;
	}
	
	/**
	 * @author wex
	 * @param node
	 * @return
	 */
	public static boolean izraz(TreeNode node){
		
		// 1. produkcija
		if(node.getChildrenAmount() == 1){
			
			// 1. provjeri(<izraz_pridruzivanja>)
			izraz_pridruzivanja(node.getChild(0));
		}
		
		// 2. produkcija
		else if(node.getChildrenAmount() == 3){
			
			// 1. provjeri(<izraz>)
			izraz(node.getChild(0));
			
			// 2. provjeri(<izraz_pridruzivanja>)
			izraz_pridruzivanja(node.getChild(2));
			
			String type = (String) Analyzator.stack.pop();
			Analyzator.stack.push(false);
			Analyzator.stack.push(type);
		}
		
		return true;
	}

}
